
<?php

require_once dirname(__FILE__) . '/../../Controller/cBitacora.php';



?>


    <div class="table-responsive">
        <table class="table table-hover table-sm" id="example">
            <thead class="">
            <tr>
                <th>Eliminar</th>
                <th>Editar</th>
                <th>Núm. de colab. </th>
                <th>ID envío OJS</th>
                <th>Convo. Num</th>
                <th>Sección</th>
                <th>Tipo colaboracion Español</th>
                <th>Tipo colaboracion Ingles</th>
                <th>% de Similitud (iThenticate)</th>
                <th>Se apega a lineamientos</th>
                <th>Autores</th>
                <th>Correos</th>
                <th>Institución</th>
                <th>Dependencia/Centro de Trabajo/</th>
                <th>Observatorio</th>
                <th>Área Académica</th>
                <th>Respresentante Comité Editorial</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($bitacoraObj->dataArticulo() as $item): ?>
                <tr class="data">
                    <td><button type="button" class="btn btn-danger" data-toggle="modal"
                                data-target="#eliminar" value="<?php echo $editar ?>">Eliminar</button></td>
                    <td><button type="button" class="btn btn-success" id="editar"
                                value="<?php echo $item->id_bitacora ?>" >Modificar</button></td>
                    <td><?php echo $item->num_colaboracion ?></td>
                    <td><?php echo $item->id_envio_ojs ?></td>
                    <td><?php echo $item->conv_num?></td>
                    <td><?php echo $item->nombre_sec?></td>
                    <td><?php echo $item->colaboracionIngles?></td>
                    <td><?php echo $item->colaboracionEspañol?></td>
                    <td><?php echo $item->similitud?></td>
                    <td><?php echo $item->lineamiento?></td>
                    <td><?php echo $item->autores?></td>
                    <td><?php echo $item->correo?></td>
                    <td><?php echo $item->institucion?></td>
                    <td><?php echo $item->dependencia_centroTrabajo?></td>
                    <td><?php echo $item->nombre_observa?></td>
                    <td><?php echo $item->nombreArea?></td>
                    <td><?php echo $item->Rep_comite_editorial?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>



   










<script>
    $(document).ready(function () {
        $('#example').DataTable({
            "language":
                {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
        });



    });
</script>
