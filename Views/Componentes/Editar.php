
<?php
require_once dirname(__FILE__) . '/../../Controller/cBitacora.php';
$bitacoraObj->setId($_REQUEST['id']);

session_start();
$_SESSION["newsession"]=$_REQUEST['id'];
//var_dump($_SESSION["newsession"]);

$editar = $bitacoraObj->dataArticulo();

//var_dump($editar[0]->id_bitacora);

?>



<form action="" id="form-art-editar" method="post">
    <div class="row">
        <div class="col-md-4">

            <input id="id" class="form-control"hidden type="number" name="id" value="<?php echo $_REQUEST['id']?>" >

            <div class="form-group">
                <label for="my-input">Número Colaboración</label>
                <input id="num_cola" class="form-control" type="number" name="num_cola" value="<?php echo $editar[0]->num_colaboracion?>" >

            </div>

            <div class="form-group">
                <label for="my-input">ID envio OJS</label>
                <input id="id_obj" class="form-control" type="text" name="id_obj" value="<?php echo $editar[0]->id_envio_ojs?>">
            </div>

            <div class="form-group">
                <label for="my-input">Año</label>
                <input id="anio" class="form-control" type="number" name="anio" value="<?php echo $editar[0]->anio?>">
            </div>

            <div class="form-group">
                <label for="my-input">Fecha de envío a autores</label>
                <input id="envioAutores" name="envioAutores"
                       value="<?php echo $editar[0]->fecha_en_Au?>"
                       class="form-control" type="text" >
            </div>

            <div class="form-group">
                <label for="my-input">Fecha de recepción de correcciones </label>
                <input id="FRCorrecciones" name="FRCorrecciones"
                       value="<?php echo $editar[0]->fecha_recep_corre?>"
                       class="form-control" type="text" >
            </div>
        </div>

<?php


$pais =$editar[0]->id_pais_fk;



?>
        <div class="col-md-4">
            <div class="form-group">
                <label for="my-select">Pais</label>
                <select id="pais" class="form-control" name="pais">
                    <?php  foreach ($bitacoraObj->paises() as $itemaPais):?>
                        <option
                            <?php echo ( strcmp(strval($itemaPais->id),$pais) == 0  )? 'selected': ''?>
                                value="<?php echo $itemaPais->id ?>" ><?php echo $itemaPais->nombre ?></option>
                    <?php endforeach; ?>
                </select>
            </div>



            <div class="form-group">
                <label for="my-input">% de Similitud (iThenticate)</label>
                <input id="iThenticate" class="form-control" type="number" name="iThenticate" value="<?php echo $editar[0]->similitud?>">
            </div>

            <div class="form-group">
                <label for="my-input">Se apega a los lineamiento</label>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="lineamientos" id="lineamientos" value="si" <?php echo ($editar[0]->lineamiento == 'si')? 'checked' : ''?>  >
                    <label class="form-check-label" for="exampleRadios1">
                        Si
                    </label>
                </div>

                <div class="form-check">
                    <input class="form-check-input" type="radio" name="lineamientos" id="lineamientos" value="no" <?php echo ($editar[0]->lineamiento == 'no')? 'checked' : ''?>>
                    <label class="form-check-label" for="exampleRadios2">
                        No
                    </label>
                </div>
            </div>

            <div class="form-group">
                <label for="my-input">Fecha de envío. Segunda ronda de revisión </label>
                <input id="FRCorrecciones2" name="FRCorrecciones2" class="form-control"
                       value="<?php echo $editar[0]->fecha_en_SR?>"
                       type="text" >
            </div>

            <div class="form-group">
                <label for="my-input">Recomendación de revisor 1 </label>
                <input id="RecRev1" name="RecRev1" class="form-control" type="text"
                       value="<?php echo $editar[0]->revisor1?>"
                >
            </div>
            <div class="form-group">
                <label for="my-input">Recomendación de revisor 2 </label>
                <input id="RecRev2" name="RecRev2" class="form-control" type="text"
                       value="<?php echo $editar[0]->revisor2?>"
                >
            </div>


        </div>




        <div class="col-md-4">
            <div class="form-group">
                <label for="my-input">Respresentante Comité Editorial</label>
                <input id="Editorial" name="Editorial" class="form-control" type="text" value="<?php echo $editar[0]->Rep_comite_editorial?>" >
            </div>



            <div class="form-group">
                <label for="my-input">Institución</label>
                <input id="institucion" name="institucion" class="form-control" type="text"
                       value="<?php echo $editar[0]->institucion?>"
                >
            </div>

            <div class="form-group">
                <label for="my-input">Numero Convenio</label>
                <input type="number" id="n_convenio" name="n_convenio" class="form-control" value="<?php echo $editar[0]->conv_num?>" >
            </div>


            <div class="form-group">
                <label for="my-input">Recomendación de revisor 3</label>
                <input id="RecRev3" name="RecRev3" class="form-control" type="text"
                       value="<?php echo $editar[0]->revisor3?>"
                >
            </div>

            <div class="form-group">
                <label for="my-input">Fecha de recepción de correcciones (segunda ronda)</label>
                <input id="FRCorrecciones3" name="FRCorrecciones3" class="form-control" type="text"
                       value="<?php echo $editar[0]->fecha_recep_corre2?>"
                >
            </div>

            <div class="form-group">
                <label for="my-input">DOI</label>
                <input id="doi" name="doi" class="form-control" type="text"
                       value="<?php echo $editar[0]->doi?>"
                >
            </div>
        </div>

        <div class="col-sm-3 ">
            <div class="form-group">
                <label for="my-input">Envíado a Comité</label>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="Enviado_Com" id="Enviado_Com" value="si"
                        <?php echo ($editar[0]->enviado_comite == 'si')? 'checked' : ''?>>
                    <label class="form-check-label" for="exampleRadios1">
                        Si
                    </label>
                </div>

                <div class="form-check">
                    <input class="form-check-input" type="radio" name="Enviado_Com" id="Enviado_Com" value="no"
                        <?php echo ($editar[0]->enviado_comite == 'no')? 'checked' : ''?>>
                    <label class="form-check-label" for="exampleRadios2">
                        No
                    </label>
                </div>
            </div>
        </div>

        <div class="col-sm-3 ">
            <div class="form-group">
                <label for="my-input">Fecha de envío a Comité Editorial</label>
                <input type="date" id="FechaEnvCE" name="FechaEnvCE" class="form-control"
                       value="<?php echo $editar[0]->fecha_envio_comiteEdi?>"
                >
            </div>
        </div>

        <div class="col-sm-3 ">
            <div class="form-group">
                <label for="my-input">Asignado por CE</label>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="Asig_C" id="Asig_C" value="si"
                        <?php echo ($editar[0]->Asignado_CE == 'si')? 'checked' : ''?>
                    >
                    <label class="form-check-label" for="exampleRadios1">
                        Si
                    </label>
                </div>

                <div class="form-check">
                    <input class="form-check-input" type="radio" name="Asig_C" id="Asig_C" value="no"
                        <?php echo ($editar[0]->Asignado_CE == 'no')? 'checked' : ''?>
                    >
                    <label class="form-check-label" for="exampleRadios2">
                        No
                    </label>
                </div>
            </div>
        </div>

        <div class="col-sm-3 ">
            <div class="form-group">
                <label for="my-input">Fecha de asignación de dictaminadores</label>
                <input type="date" id="FechaAsigD" name="FechaAsigD" class="form-control"
                       value="<?php echo $editar[0]->fecha_asig_dictadores?>"
                >
            </div>
        </div>

        <div class="col-sm-4 p-3">
            <div class="form-group">
                <label for="my-input">Fecha de recepción </label>
                <input type="date" id="FechaRecepcion" name="FechaRecepcion" class="form-control"
                <input type="number" id="n_convenio" name="n_convenio" class="form-control" value="<?php echo $editar[0]->fecha_recepcion?>" >
            </div>
        </div>



        <div class="col-sm-4 p-3">
            <div class="form-group">
                <label for="my-input">status</label>
                <select id="status" class="form-control" name="status">
                    <option value="Autores" <?php echo ($editar[0]->status == 'Autores')? 'selected' : ''?> >Autores</option>
                    <option value="En revisión"
                        <?php echo ($editar[0]->status == 'En revisión')? 'selected' : ''?>
                    >En revisión</option>
                    <option value="Rechazado"
                        <?php echo ($editar[0]->status == 'Rechazado')? 'selected' : ''?>
                    >Rechazado</option>
                    <option value="En asignación"
                        <?php echo ($editar[0]->status == 'En asignación')? 'selected' : ''?>
                    >En asignación</option>
                    <option value="Acetados"
                        <?php echo ($editar[0]->status == 'Acetados')? 'selected' : ''?>
                    >Acetados</option>
                    <option value="Devueltos"
                        <?php echo ($editar[0]->status == 'Devueltos')? 'selected' : ''?>
                    >Devueltos</option>
                </select>
            </div>
        </div>


        <div class="col-sm-4 ">
            <div class="form-group">
                <label for="my-input">Proceso completo (Completo)</label>
                <select id="pc_c" class="form-control" name="pc_c">
                    <option value="Devuelto"
                        <?php echo ($editar[0]->proceso == 'Devuelto')? 'selected' : ''?>
                    >Devuelto</option>
                    <option value="SI"
                        <?php echo ($editar[0]->proceso == 'SI')? 'selected' : ''?>
                    >SI</option>
                    <option value="NO"
                        <?php echo ($editar[0]->proceso == 'NO')? 'selected' : ''?>
                    >NO</option>
                    <option value="Completo"
                        <?php echo ($editar[0]->proceso == 'Completo')? 'selected' : ''?>
                    >Completo</option>
                </select>
            </div>
        </div>




        <div class="col-sm-12 p-3">
            <label for="my-input">Título Colaboración</label>
            <div class="row">
                <div class="col-6 col-sm-6">
                    <input id="t-col-E" class="form-control" type="text" name="t-col-E" placeholder="Español" value="<?php echo $editar[0]->colaboracionEspañol?>">
                </div>
                <div class="col-6 col-sm-6">
                    <input id="t_col-I" class="form-control" type="text" name="t-col-I" placeholder="Ingles" value="<?php echo $editar[0]->colaboracionIngles?>">
                </div>
            </div>
        </div>


        <div class="col-sm-4 p-3">
            <div class="form-group">
                <label for="my-input">Autores</label>
                <textarea class="form-control" id="autores" name="autores" rows="3"><?php echo $editar[0]->autores?></textarea>
            </div>
        </div>




        <div class="col-sm-4 p-3">
            <div class="form-group">
                <label for="my-input">Correos</label>
                <textarea class="form-control" id="correo" name="correo" rows="3"><?php echo $editar[0]->correo?></textarea>

            </div>
        </div>

        <div class="col-sm-4 p-3">
            <div class="form-group">
                <label for="my-input">Dependencia/Centro de Trabajo/</label>
                <textarea  class="form-control" id="dependencia" name="dependencia" rows="3"><?php echo $editar[0]->dependencia_centroTrabajo?></textarea>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group" id="seccion-load-edi">
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group" id="obc-load-edit">
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group" id="AC-edit">
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group" id="TP-edit">
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="my-input">Notas</label>
                <textarea class="form-control" id="nota" name="nota" rows="3"><?php echo $editar[0]->nota?></textarea>

            </div>
        </div>
        <hr>


        <div class="col-md-6">
            <div class="form-group" id="TP-edit">
            </div>
        </div>




        <div class="col-12 ">
            <h3 class="text-center"> Revisiones del articulo</h3>
            <label> Nueva revision </label>
            <button type="button" class="btn btn-outline-primary btn-sm" id="ModalR" value="<?php echo $_REQUEST['id'] ?>"><i class="fas fa-external-link-alt"></i></button>
        </div>



        <div class="col-md-12" id="tabla-revision">
           
        </div>



    </div>














    




    



</form>



<script>
    $(document).ready(function () {
        $('#seccion-load-edi').load('Views/Selectores/seccion.php?id=<?php echo $editar[0]->id_seccion_fk ?>');

        $('#obc-load-edit').load('Views/Selectores/observatorio.php?id=<?php echo $editar[0]->id_obs_fk?>');
        $('#AC-edit').load('Views/Selectores/AreaAcademica.php?id=<?php echo $editar[0]->id_area_cademica_fk ?>');
        $('#TP-edit').load('Views/Selectores/colaboracion.php?id=<?php echo $editar[0]->id_tipo_col_fk ?>');

        $('#tabla-revision').load('Views/Tables/tabla-Revision.php?id=<?php echo $_SESSION["newsession"] ?>');

    });
</script>



