<!--Modal Alta-->



<div class="col-12 m-2 p-1">
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary " id="agregar-Art">
        Agregar
    </button>


    <div class="modal fade  " id="modal-Art" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
         aria-hidden="true">
        <div class="modal-dialog  modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Articulo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <form action="" id="form-art" method="post">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="my-input">Número Colaboración</label>
                                        <input id="num_cola" class="form-control" type="number" name="num_cola">

                                    </div>

                                    <div class="form-group">
                                        <label for="my-input">ID envio OJS</label>
                                        <input id="id_obj" class="form-control" type="text" name="id_obj">
                                    </div>

                                    <div class="form-group">
                                        <label for="my-input">Año</label>
                                        <input id="anio" class="form-control" type="number" name="anio">
                                    </div>


                                        <div class="form-group">
                                            <label for="my-input">Recibido despues del convenio</label>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="rdConvenio" id="rdConvenio" value="si" >
                                                <label class="form-check-label" for="exampleRadios1">
                                                    Si
                                                </label>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="rdConvenio" id="rdConvenio" value="no">
                                                <label class="form-check-label" for="exampleRadios2">
                                                    No
                                                </label>
                                            </div>
                                        </div>




                                    <div class="form-group">
                                        <label for="my-input">Fecha de envío a autores</label>
                                        <input id="envioAutores" name="envioAutores" class="form-control" type="text" >
                                    </div>

                                    <div class="form-group">
                                        <label for="my-input">Fecha de recepción de correcciones </label>
                                        <input id="FRCorrecciones" name="FRCorrecciones" class="form-control" type="text" >
                                    </div>






                                </div>



                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="my-select">Pais</label>
                                        <select id="pais" class="form-control" name="pais">
                                            <?php  foreach ($bitacoraObj->paises() as $itemaPais):?>
                                                <option value="<?php echo $itemaPais->id ?>" ><?php echo $itemaPais->nombre ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>



                                    <div class="form-group">
                                        <label for="my-input">% de Similitud (iThenticate)</label>
                                        <input id="iThenticate" class="form-control" type="number" name="iThenticate">
                                    </div>

                                    <div class="form-group">
                                        <label for="my-input">Se apega a los lineamiento</label>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="lineamientos" id="lineamientos" value="si" >
                                            <label class="form-check-label" for="exampleRadios1">
                                                Si
                                            </label>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="lineamientos" id="lineamientos" value="no">
                                            <label class="form-check-label" for="exampleRadios2">
                                                No
                                            </label>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label for="my-input">Fecha de envío. Segunda ronda de revisión </label>
                                        <input id="FRCorrecciones2" name="FRCorrecciones2" class="form-control" type="text" >
                                    </div>

                                    <div class="form-group">
                                        <label for="my-input">Recomendación de revisor 1 </label>
                                        <input id="RecRev1" name="RecRev1" class="form-control" type="text" >
                                    </div>
                                    <div class="form-group">
                                        <label for="my-input">Recomendación de revisor 2 </label>
                                        <input id="RecRev2" name="RecRev2" class="form-control" type="text" >
                                    </div>




                                </div>


                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="my-input">Respresentante Comité Editorial</label>
                                        <input id="Editorial" name="Editorial" class="form-control" type="text" >
                                    </div>



                                    <div class="form-group">
                                        <label for="my-input">Institución</label>
                                        <input id="institucion" name="institucion" class="form-control" type="text" >
                                    </div>

                                    <div class="form-group">
                                        <label for="my-input">Numero Convenio</label>
                                        <input type="text" id="n_convenio" name="n_convenio" class="form-control"  >
                                    </div>

                                    <div class="form-group">
                                        <label for="my-input">Recomendación de revisor 3</label>
                                        <input id="RecRev3" name="RecRev3" class="form-control" type="text" >
                                    </div>

                                    <div class="form-group">
                                        <label for="my-input">Fecha de recepción de correcciones (segunda ronda)</label>
                                        <input id="FRCorrecciones3" name="FRCorrecciones3" class="form-control" type="text" >
                                    </div>

                                    <div class="form-group">
                                        <label for="my-input">DOI</label>
                                        <input id="doi" name="doi" class="form-control" type="text" >
                                    </div>
                                </div>




                                <div class="col-sm-3 ">
                                    <div class="form-group">
                                        <label for="my-input">Envíado a Comité</label>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="Enviado_Com" id="Enviado_Com" value="si" >
                                            <label class="form-check-label" for="exampleRadios1">
                                                Si
                                            </label>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="Enviado_Com" id="Enviado_Com" value="no">
                                            <label class="form-check-label" for="exampleRadios2">
                                                No
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3 ">
                                    <div class="form-group">
                                        <label for="my-input">Fecha de envío a Comité Editorial</label>
                                        <input type="date" id="FechaEnvCE" name="FechaEnvCE" class="form-control"  >
                                    </div>
                                </div>

                                <div class="col-sm-3 ">
                                    <div class="form-group">
                                        <label for="my-input">Asignado por CE</label>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="Asig_C" id="Asig_C" value="si" >
                                            <label class="form-check-label" for="exampleRadios1">
                                                Si
                                            </label>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="Asig_C" id="Asig_C" value="no">
                                            <label class="form-check-label" for="exampleRadios2">
                                                No
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3 ">
                                    <div class="form-group">
                                        <label for="my-input">Fecha de asignación de dictaminadores</label>
                                        <input type="date" id="FechaAsigD" name="FechaAsigD" class="form-control"  >
                                    </div>
                                </div>

                                <div class="col-sm-4 p-3">
                                    <div class="form-group">
                                        <label for="my-input">Fecha de recepción </label>
                                        <input type="date" id="FechaRecepcion" name="FechaRecepcion" class="form-control"  >
                                    </div>
                                </div>



                                <div class="col-sm-4 p-3">
                                    <div class="form-group">
                                        <label for="my-input">status</label>
                                        <select id="status" class="form-control" name="status">
                                            <option value="Autores">Autores</option>
                                            <option value="En revisión">En revisión</option>
                                            <option value="Rechazado">Rechazado</option>
                                            <option value="En asignación">En asignación</option>
                                            <option value="Acetados">Acetados</option>
                                            <option value="Devueltos">Devueltos</option>
                                        </select>

                                    </div>
                                </div>


                                <div class="col-sm-4 p-3">
                                    <div class="form-group">
                                        <label for="my-input">Proceso completo (Completo)</label>

                                        <select id="pc_c" class="form-control" name="pc_c">
                                            <option value="Devuelto">Devuelto</option>
                                            <option value="SI">SI</option>
                                            <option value="NO">NO</option>
                                            <option value="Completo">Completo</option>
                                        </select>




                                    </div>
                                </div>

                                <div class="col-sm-12 p-3">
                                    <label for="my-input">Título Colaboración</label>
                                    <div class="row">
                                        <div class="col-6 col-sm-6">
                                            <input id="t-col-E" class="form-control" type="text" name="t-col-E" placeholder="Español">
                                        </div>
                                        <div class="col-6 col-sm-6">
                                            <input id="t_col-I" class="form-control" type="text" name="t-col-I" placeholder="Ingles">
                                        </div>
                                    </div>
                                </div>




                                <div class="col-sm-4 p-3">
                                <div class="form-group">
                                    <label for="my-input">Autores</label>
                                    <textarea class="form-control" id="autores" name="autores" rows="3"></textarea>
                                </div>
                                </div>




                                <div class="col-sm-4 p-3">
                                    <div class="form-group">
                                        <label for="my-input">Correos</label>
                                        <textarea class="form-control" id="correo" name="correo" rows="3"></textarea>

                                    </div>
                                </div>

                                <div class="col-sm-4 p-3">
                                <div class="form-group">
                                    <label for="my-input">Dependencia/Centro de Trabajo/</label>
                                    <textarea  class="form-control" id="dependencia" name="dependencia" rows="3"></textarea>

                                </div>
                                </div>



                                <div class="col-md-6">
                                    <div class="form-group" id="seccion-load">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group" id="obc-load">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group" id="AC">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group" id="TP">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="my-input">Notas</label>
                                        <textarea class="form-control" id="nota" name="nota" rows="3"></textarea>

                                    </div>
                                </div>



                                <br>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <div id="mensaje"></div>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="AddArticulo">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <div id="my-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <p>Content</p>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Alta -->



</div>

<!--Modal editar-->
<div class="modal fade" id="editar-Ar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar Articulo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="editar-articulo">
                ...
            </div>
            <div class="modal-footer">
                <div id="mensaje-edit-art"></div>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>
                <button type="button" class="btn btn-primary" id="EditArticulo">Guardar</button>


            </div>
        </div>
    </div>
</div>
<!--//Modal editar-->

<!--Modal eliminar-->
<div class="modal fade" id="eliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!--//Modal editar-->


<!--Seccion -->


<div class="modal fade" id="seccion-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Registro Sección</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body body-seccion" id="modal-body-seccio">
            <form method="get" action="" id="form_seccion">
            <div class="form-group">
              <label for=""></label>
              <input type="text"
                class="form-control" name="nuevaSeccion" id="nuevaSeccion" aria-describedby="helpId" placeholder="">
              <small id="helpId" class="form-text text-muted">Sección</small>
            </div>                
            </form>
                <?php include 'loading.php' ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>
            </div>
        </div>
    </div>
</div>


<!--observatorio -->


<div class="modal fade" id="observatorio-1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Registro Observatorio</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body body-obv" id="modal-body-obv">
                <form method="get" action="" id="form_obv">
                    <div class="form-group">
                        <label for=""></label>
                        <input type="text"
                               class="form-control" name="nuevoObv" id="nuevoObv" aria-describedby="helpId" placeholder="">
                        <small id="helpId" class="form-text text-muted">Observatorio</small>
                    </div>
                </form>
                <?php include 'loading.php' ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>

            </div>
        </div>
    </div>
</div>



<!--tipo colaboracion -->


<div class="modal fade" id="modal_tipo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Registro Tipo Colaboración</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body body-obv" id="modal-body-tipo">
                <?php include 'loading.php' ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>
            </div>
        </div>
    </div>
</div>


<!--Area Academica -->
<div class="modal fade" id="modal_AC" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Registro Área Academica</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body body-obv" id="modal-body-AA">
                <?php include 'loading.php' ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>
            </div>
        </div>
    </div>
</div>

<!--Revision ADD-->
<div class="modal fade" id="modal_revision" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Registro revisión</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body body-obv" id="modal-body-revision">
                <?php include 'loading.php' ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>
                <button type="button" class="btn btn-primary" id="AddRevision">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!--Revision Edit-->
<div class="modal fade" id="modal_revision_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar revisión</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body body-obv" id="modal-body-revision-edit">
                <?php include 'loading.php' ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>
                <button type="button" class="btn btn-primary" id="EditRevision">Guardar</button>
            </div>
        </div>
    </div>
</div>



<script>
    $('.body-seccion').load('Views/Tables/crudSeccion.php');
    $('#seccion-load').load('Views/Selectores/seccion.php');
    $('#obc-load').load('Views/Selectores/observatorio.php');
    $('#AC').load('Views/Selectores/AreaAcademica.php');
    $('#TP').load('Views/Selectores/colaboracion.php');
</script>



