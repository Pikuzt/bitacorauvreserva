<?php
require_once dirname(__FILE__) . '/../../../Controller/cBitacora.php';
$datos = $bitacoraObj->filtradorTabla();
var_dump(intval($_REQUEST['buscar']));

?>


<main>
    <div class="container">
        <div class="row">
            <div class="col-4 m-0">
                <form id="form_buscar">
                    <div class="input-group input-group-sm mb-3" >
                        <input id="buscar" name="buscar" type="text" class="form-control">
                    </div>
                </form>
            </div>
            <div class="col-4 m-0">
                <button type="button" class="btn btn-primary btn-sm" id="buscar-btn"><i class="fas fa-search"></i> buscar</button>
            </div>
            <div id="data-tabla"></div>
        </div>
    </div>
</main>


<script>
    $('#data-tabla').load('Views/AvancesRevista/Componentes/tabla.php');
</script>
