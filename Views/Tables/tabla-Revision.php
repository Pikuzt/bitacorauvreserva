<?php
require_once dirname(__FILE__) . '/../../Controller/cBitacora.php';

session_start();
$bitacoraObj->setId($_SESSION["newsession"]);

?>
<div class="col-12">
    <div class="table-responsive">
        <table class="table table-hover table-sm" id="datos_TC">
            <thead >
            <tr>
                <th>Revision</th>
                <th>Fecha de envío a dictamen </th>
                <th>Fecha recepción revisión </th>
                <th>Recomendación de revisor</th>
                <th>Constancia enviada</th>
                <th>Eliminar</th>
                <th>Modificar</th>
            </tr>

            </thead>
            <tbody id="tablita">
            <?php foreach ($bitacoraObj->DataRevision() as $item): ?>
                <tr class="data">
                    <td><?php echo $item->nombre_revision?></td>
                    <td><?php echo $item->fecha_envio ?></td>
                    <td><?php echo $item->fecha_recepcion ?></td>
                    <td><?php echo $item->recomendacion ?></td>
                    <td><?php echo $item->constancia_enviada ?></td>
                    <td><button type="button" class="btn btn-danger" data-toggle="modal"
                                data-target="#eliminarR" value="<?php echo $item->id_bitacora ?>">Eliminar</button></td>
                    <td><button type="button" class="btn btn-success" id="editarR"
                                value="<?php echo $item->id_revision ?>" >Modificar</button></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>

</div>



<script>
    $(document).ready(function () {
        $('#datos_TC').DataTable({
            "language":
                {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
        });



    });
</script>