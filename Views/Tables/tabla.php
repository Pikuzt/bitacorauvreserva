<?php

require_once dirname(__FILE__) . '/../../Controller/cBitacora.php';

?>


<div class="col-12">
    <div class="table-responsive">
        <table class="table table-hover table-sm" id="tableSeccion">
            <thead >
            <tr>
                <th>N°</th>
                <th>Nombre</th>

            </tr>
            </thead>


            <tbody id="tablita">
            <?php foreach ($bitacoraObj->dataSeccion() as $seccion): ?>
                <tr class="data">
                    <td><?php echo $seccion->id_seccion ?></td>
                    <td><?php echo $seccion->nombre_sec ?></td>

                </tr>

            <?php endforeach; ?>
            </tbody>
        </table>
    </div>

</div>



<script>
    $(document).ready(function () {
        $('#tableSeccion').DataTable({
            "language":
                {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
        });



    });
</script>