<?php
require_once dirname(__FILE__) . '/../../Controller/cBitacora.php';
?>


<div class="col-12">
    <div class="table-responsive">
        <table class="table table-hover table-sm" id="datos_AA">
            <thead >
            <tr>
                <th>N°</th>
                <th>Nombre</th>
            </tr>
            </thead>
            <tbody id="tablita">
            <?php foreach ($bitacoraObj->dataAA() as $item): ?>
                <tr class="data">
                    <td><?php echo $item->id_area_academica?></td>
                    <td><?php echo $item->nombreArea ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>

</div>



<script>
    $(document).ready(function () {
        $('#datos_AA').DataTable({
            "language":
                {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
        });



    });
</script>