<?php
require_once dirname(__FILE__) . '/../../Controller/cBitacora.php';
$id = !empty($_REQUEST['id'])? $_REQUEST['id']: 0 ;



?>


<style>
    .btn-uv{
        position: relative;
        top: -35px;
        left: 104%;
        z-index: 5;
    }
</style>


<div class="row">
    <div class="col-10">
        <label for="my-input">Tipo colaboración</label>
        <select id="tc" class="form-control" name="tc">
            <?php  foreach ($bitacoraObj->dataTipo() as $itemaTipo):?>
                <?php if(!empty($_REQUEST['id'])): ?>
                    <option
                        <?php echo ( strcmp(strval($itemaTipo->id_tipo_Colaboracion),$_REQUEST['id']) == 0  )? 'selected': ''?>
                            value="<?php echo $itemaTipo->id_tipo_Colaboracion ?>" ><?php echo $itemaTipo->nombre_colaboracion ?></option>
                <?php else:?>
                    <option value="<?php echo $itemaTipo->id_tipo_Colaboracion ?>" ><?php echo $itemaTipo->nombre_colaboracion ?></option>
                <?php endif; ?>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="col-10">
        <a  class="btn-uv btn btn-outline-secondary btn-sm" id="modal-tipo" data-target="#crearTipo"  >+</a>

    </div>

</div>
