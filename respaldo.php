<main>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="table-resposive">
                    <table class="table table-bordered" id="ejemplo">
                        <thead>
                        <tr class="title_uv text-center" >
                            <th scope="col" rowspan="2">Áreas académicas</th>
                            <th scope="col" rowspan="2">Recibidos de convocatorias anteriores </th>
                            <th scope="col" rowspan="2">Recibidos de convocatoria actual </th>
                            <th scope="col" rowspan="2">Recibidos despues convocatoria </th>
                            <th scope="col" rowspan="2">Total</th>
                            <th  class="text-center" colspan="5">Estados
                            <th scope="col" rowspan="2">Sub Total</th>
                            <th scope="col" rowspan="2">Proceso completo</th>
                            <th scope="col" rowspan="2">Aceptados</th>

                            </th>
                        </tr>
                        <tr>
                            <th scope="col" class="sub-title-uv">Devueltos</th>
                            <th scope="col" class="sub-title-uv">Enviados a comité</th>
                            <th scope="col" class="sub-title-uv">En revision</th>
                            <th scope="col" class="sub-title-uv">En correciones</th>
                            <th scope="col" class="sub-title-uv">Rechazados</th>
                        </tr>
                        </thead>


                        <tbody>

                        <tr class="text-center">
                            <th scope="row">Artes</th>
                            <td><?php echo count( Ant_Act_des($datos,10,1)) ?> </td>
                            <td><?php echo count( Ant_Act_des($datos,11,1)) ?> </td>
                            <td><?php echo count( Ant_Act_des($datos,0,1)) ?> </td>
                            <td><?php echo
                                    count( Ant_Act_des($datos,10,1)) +
                                    count(Ant_Act_des($datos,11,1)) +
                                    count(Ant_Act_des($datos,0,1))?>
                            </td>
                            <td><?php echo count(devueltosEstados($datos,1)) ?></td>
                            <td><?php echo count(comiteEstado($datos,1)) ?></td>
                            <td><?php echo count(estatusEstado($datos,1,'En revisión')) ?></td>
                            <td><?php echo count(estatusEstado($datos,1,'Autores')) ?></td>
                            <td><?php echo count(estatusEstado($datos,1,'Rechazado')) ?></td>
                            <td><?php echo
                                    count(devueltosEstados($datos,1))+
                                    count(comiteEstado($datos,1))+
                                    count(estatusEstado($datos,1,'En revisión'))+
                                    count(estatusEstado($datos,1,'Autores'))+
                                    count(estatusEstado($datos,1,'Rechazado'))
                                ?>
                            </td>
                            <td><?php echo count(procesoCompleto($datos,1,'Completo')) ?></td>
                            <td><?php echo count(estatusEstado($datos,1,'Acetados')) ?></td>

                        </tr>
                        <tr class="text-center">
                            <th scope="row">Biológico Agropecuaria</th>
                            <td><?php echo count( Ant_Act_des($datos,10,2)) ?> </td>
                            <td><?php echo count( Ant_Act_des($datos,11,2)) ?> </td>
                            <td><?php echo count( Ant_Act_des($datos,0,2)) ?> </td>
                            <td><?php echo
                                    count( Ant_Act_des($datos,10,2)) +
                                    count(Ant_Act_des($datos,11,2)) +
                                    count(Ant_Act_des($datos,0,2))?>
                            </td>
                            <td><?php echo count(devueltosEstados($datos,2)) ?></td>
                            <td><?php echo count(comiteEstado($datos,2)) ?></td>
                            <td><?php echo count(estatusEstado($datos,2,'En revisión')) ?></td>
                            <td><?php echo count(estatusEstado($datos,2,'Autores')) ?></td>
                            <td><?php echo count(estatusEstado($datos,2,'Rechazado')) ?></td>
                            <td><?php echo
                                    count(devueltosEstados($datos,2))+
                                    count(comiteEstado($datos,2))+
                                    count(estatusEstado($datos,2,'En revisión'))+
                                    count(estatusEstado($datos,2,'Autores'))+
                                    count(estatusEstado($datos,2,'Rechazado'))
                                ?>
                            </td>
                            <td><?php echo count(procesoCompleto($datos,2,'Completo')) ?></td>
                            <td><?php echo count(estatusEstado($datos,2,'Acetados')) ?></td>
                        </tr>
                        <tr class="text-center">
                            <th scope="row">Ciencias de la Salud</th>
                            <td><?php echo count( Ant_Act_des($datos,10,3)) ?> </td>
                            <td><?php echo count( Ant_Act_des($datos,11,3)) ?> </td>
                            <td><?php echo count( Ant_Act_des($datos,0,3)) ?> </td>
                            <td><?php echo
                                    count( Ant_Act_des($datos,10,3)) +
                                    count(Ant_Act_des($datos,11,3)) +
                                    count(Ant_Act_des($datos,0,3))?>
                            </td>
                            <td><?php echo count(devueltosEstados($datos,3)) ?></td>
                            <td><?php echo count(comiteEstado($datos,3)) ?></td>
                            <td><?php echo count(estatusEstado($datos,3,'En revisión')) ?></td>
                            <td><?php echo count(estatusEstado($datos,3,'Autores')) ?></td>
                            <td><?php echo count(estatusEstado($datos,3,'Rechazado')) ?></td>
                            <td><?php echo
                                    count(devueltosEstados($datos,3))+
                                    count(comiteEstado($datos,3))+
                                    count(estatusEstado($datos,3,'En revisión'))+
                                    count(estatusEstado($datos,3,'Autores'))+
                                    count(estatusEstado($datos,3,'Rechazado'))
                                ?>
                            </td>
                            <td><?php echo count(procesoCompleto($datos,3,'Completo')) ?></td>
                            <td><?php echo count(estatusEstado($datos,3,'Acetados')) ?></td>

                        </tr>
                        <tr class="text-center">
                            <th scope="row">Económico Administrativa</th>
                            <td><?php echo count( Ant_Act_des($datos,10,4)) ?> </td>
                            <td><?php echo count( Ant_Act_des($datos,11,4)) ?> </td>
                            <td><?php echo count( Ant_Act_des($datos,0,4)) ?> </td>
                            <td><?php echo
                                    count( Ant_Act_des($datos,10,4)) +
                                    count(Ant_Act_des($datos,11,4)) +
                                    count(Ant_Act_des($datos,0,4))?>
                            </td>
                            <td><?php echo count(devueltosEstados($datos,4)) ?></td>
                            <td><?php echo count(comiteEstado($datos,4)) ?></td>
                            <td><?php echo count(estatusEstado($datos,4,'En revisión')) ?></td>
                            <td><?php echo count(estatusEstado($datos,4,'Autores')) ?></td>
                            <td><?php echo count(estatusEstado($datos,4,'Rechazado')) ?></td>
                            <td><?php echo
                                    count(devueltosEstados($datos,4))+
                                    count(comiteEstado($datos,4))+
                                    count(estatusEstado($datos,4,'En revisión'))+
                                    count(estatusEstado($datos,4,'Autores'))+
                                    count(estatusEstado($datos,4,'Rechazado'))
                                ?>
                            </td>
                            <td><?php echo count(procesoCompleto($datos,4,'Completo')) ?></td>
                            <td><?php echo count(estatusEstado($datos,4,'Acetados')) ?></td>
                        </tr>
                        <tr class="text-center">
                            <th scope="row">Humanidades</th>
                            <td><?php echo count( Ant_Act_des($datos,10,5)) ?> </td>
                            <td><?php echo count( Ant_Act_des($datos,11,5)) ?> </td>
                            <td><?php echo count( Ant_Act_des($datos,0,5)) ?> </td>
                            <td><?php echo
                                    count( Ant_Act_des($datos,10,5)) +
                                    count(Ant_Act_des($datos,11,5)) +
                                    count(Ant_Act_des($datos,0,5))?>
                            </td>
                            <td><?php echo count(devueltosEstados($datos,5)) ?></td>
                            <td><?php echo count(comiteEstado($datos,5)) ?></td>
                            <td><?php echo count(estatusEstado($datos,5,'En revisión')) ?></td>
                            <td><?php echo count(estatusEstado($datos,5,'Autores')) ?></td>
                            <td><?php echo count(estatusEstado($datos,5,'Rechazado')) ?></td>
                            <td><?php echo
                                    count(devueltosEstados($datos,5))+
                                    count(comiteEstado($datos,5))+
                                    count(estatusEstado($datos,5,'En revisión'))+
                                    count(estatusEstado($datos,5,'Autores'))+
                                    count(estatusEstado($datos,5,'Rechazado'))
                                ?>
                            </td>
                            <td><?php echo count(procesoCompleto($datos,5,'Completo')) ?></td>
                            <td><?php echo count(estatusEstado($datos,5,'Acetados')) ?></td>
                        </tr>
                        <tr class="text-center">
                            <th scope="row">Técnica</th>
                            <td><?php echo count( Ant_Act_des($datos,10,6)) ?> </td>
                            <td><?php echo count( Ant_Act_des($datos,11,6)) ?> </td>
                            <td><?php echo count( Ant_Act_des($datos,0,6)) ?> </td>
                            <td><?php echo
                                    count( Ant_Act_des($datos,10,6)) +
                                    count(Ant_Act_des($datos,11,6)) +
                                    count(Ant_Act_des($datos,0,6))?>
                            </td>
                            <td><?php echo count(devueltosEstados($datos,6)) ?></td>
                            <td><?php echo count(comiteEstado($datos,6)) ?></td>
                            <td><?php echo count(estatusEstado($datos,6,'En revisión')) ?></td>
                            <td><?php echo count(estatusEstado($datos,6,'Autores')) ?></td>
                            <td><?php echo count(estatusEstado($datos,6,'Rechazado')) ?></td>
                            <td><?php echo
                                    count(devueltosEstados($datos,6))+
                                    count(comiteEstado($datos,6))+
                                    count(estatusEstado($datos,6,'En revisión'))+
                                    count(estatusEstado($datos,6,'Autores'))+
                                    count(estatusEstado($datos,6,'Rechazado'))
                                ?>
                            </td>
                            <td><?php echo count(procesoCompleto($datos,6,'Completo')) ?></td>
                            <td><?php echo count(estatusEstado($datos,6,'Acetados')) ?></td>
                        </tr>

                        </tbody>

                        <tfoot>
                        <tr class="text-center">
                            <th scope="row">Total</th>
                            <th scope="row"></th>
                            <th scope="row"></th>
                            <th scope="row"></th>
                            <th scope="row"></th>
                            <th scope="row"></th>
                            <th scope="row"></th>
                            <th scope="row"></th>
                            <th scope="row"></th>
                            <th scope="row"></th>
                            <th scope="row"></th>
                            <th scope="row"></th>
                            <th scope="row"></th>



                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>






        </div>
    </div>



    <div> </div>
</main>


<script>


    //Ejecuto la función al cargar la página
    $(document).ready(function()
    {


        //Defino los totales de mis 2 columnas en 0
        var total_col1 = 0;
        var total_col2 = 0;
        var total_col3 = 0;
        var total_col4 = 0;
        var total_col5 = 0;
        var total_col6 = 0;
        var total_col7 = 0;
        var total_col8 = 0;
        var total_col9 = 0;
        var total_col10 = 0;
        var total_col11 = 0;
        var total_col12 = 0;
        //Recorro todos los tr ubicados en el tbody
        $('#ejemplo tbody').find('tr').each(function (i, el) {

            //Voy incrementando las variables segun la fila ( .eq(0) representa la fila 1 )
            total_col1 += parseFloat($(this).find('td').eq(0).text());
            total_col2 += parseFloat($(this).find('td').eq(1).text());
            total_col3 += parseFloat($(this).find('td').eq(2).text());
            total_col4 += parseFloat($(this).find('td').eq(3).text());
            total_col5 += parseFloat($(this).find('td').eq(4).text());
            total_col6 += parseFloat($(this).find('td').eq(5).text());
            total_col7 += parseFloat($(this).find('td').eq(6).text());
            total_col8 += parseFloat($(this).find('td').eq(7).text());
            total_col9 += parseFloat($(this).find('td').eq(8).text());
            total_col10 += parseFloat($(this).find('td').eq(9).text());
            total_col11 += parseFloat($(this).find('td').eq(10).text());
            total_col12 += parseFloat($(this).find('td').eq(11).text());

        });
        //Muestro el resultado en el th correspondiente a la columna
        $('#ejemplo tfoot tr th').eq(1).text( total_col1);
        $('#ejemplo tfoot tr th').eq(2).text( total_col2);
        $('#ejemplo tfoot tr th').eq(3).text( total_col3);
        $('#ejemplo tfoot tr th').eq(4).text( total_col4);
        $('#ejemplo tfoot tr th').eq(5).text( total_col5);
        $('#ejemplo tfoot tr th').eq(6).text( total_col6);
        $('#ejemplo tfoot tr th').eq(7).text( total_col7);
        $('#ejemplo tfoot tr th').eq(8).text( total_col8);
        $('#ejemplo tfoot tr th').eq(9).text( total_col9);
        $('#ejemplo tfoot tr th').eq(10).text( total_col10);
        $('#ejemplo tfoot tr th').eq(11).text( total_col11);
        $('#ejemplo tfoot tr th').eq(12).text( total_col12);



    });
</script>





----------------------------------------------------------
nuevos valores


<div class="col-12">
    <div class="table-resposive">
        <table class="table table-bordered" id="ejemplo">
            <thead>
            <tr class="title_uv text-center" >
                <th scope="col" rowspan="2">Áreas académicas</th>
                <th scope="col" rowspan="2">Recibidos de convocatorias anteriores </th>
                <th scope="col" rowspan="2">Recibidos de convocatoria actual </th>
                <th scope="col" rowspan="2">Recibidos despues convocatoria </th>
                <th scope="col" rowspan="2">Total</th>
                <th  class="text-center" colspan="5">Estados
                <th scope="col" rowspan="2">Sub Total</th>
                <th scope="col" rowspan="2">Proceso completo</th>
                <th scope="col" rowspan="2">Aceptados</th>

                </th>
            </tr>
            <tr>
                <th scope="col" class="sub-title-uv">Devueltos</th>
                <th scope="col" class="sub-title-uv">Enviados a comité</th>
                <th scope="col" class="sub-title-uv">En revision</th>
                <th scope="col" class="sub-title-uv">En correciones</th>
                <th scope="col" class="sub-title-uv">Rechazados</th>
            </tr>
            </thead>


            <tbody>

            <tr class="text-center">
                <th scope="row">Artes</th>
                <td><?php echo count( Ant_Act_des($datos,10,1)) ?> </td>
                <td><?php echo count( Ant_Act_des($datos,11,1)) ?> </td>
                <td><?php echo count( Ant_Act_des($datos,0,1)) ?> </td>
                <td><?php echo
                        count( Ant_Act_des($datos,10,1)) +
                        count(Ant_Act_des($datos,11,1)) +
                        count(Ant_Act_des($datos,0,1))?>
                </td>
                <td><?php echo count(devueltosEstados($datos,1)) ?></td>
                <td><?php echo count(comiteEstado($datos,1)) ?></td>
                <td><?php echo count(estatusEstado($datos,1,'En revisión')) ?></td>
                <td><?php echo count(estatusEstado($datos,1,'Autores')) ?></td>
                <td><?php echo count(estatusEstado($datos,1,'Rechazado')) ?></td>
                <td><?php echo
                        count(devueltosEstados($datos,1))+
                        count(comiteEstado($datos,1))+
                        count(estatusEstado($datos,1,'En revisión'))+
                        count(estatusEstado($datos,1,'Autores'))+
                        count(estatusEstado($datos,1,'Rechazado'))
                    ?>
                </td>
                <td><?php echo count(procesoCompleto($datos,1,'Completo')) ?></td>
                <td><?php echo count(estatusEstado($datos,1,'Acetados')) ?></td>

            </tr>
            <tr class="text-center">
                <th scope="row">Biológico Agropecuaria</th>
                <td><?php echo count( Ant_Act_des($datos,10,2)) ?> </td>
                <td><?php echo count( Ant_Act_des($datos,11,2)) ?> </td>
                <td><?php echo count( Ant_Act_des($datos,0,2)) ?> </td>
                <td><?php echo
                        count( Ant_Act_des($datos,10,2)) +
                        count(Ant_Act_des($datos,11,2)) +
                        count(Ant_Act_des($datos,0,2))?>
                </td>
                <td><?php echo count(devueltosEstados($datos,2)) ?></td>
                <td><?php echo count(comiteEstado($datos,2)) ?></td>
                <td><?php echo count(estatusEstado($datos,2,'En revisión')) ?></td>
                <td><?php echo count(estatusEstado($datos,2,'Autores')) ?></td>
                <td><?php echo count(estatusEstado($datos,2,'Rechazado')) ?></td>
                <td><?php echo
                        count(devueltosEstados($datos,2))+
                        count(comiteEstado($datos,2))+
                        count(estatusEstado($datos,2,'En revisión'))+
                        count(estatusEstado($datos,2,'Autores'))+
                        count(estatusEstado($datos,2,'Rechazado'))
                    ?>
                </td>
                <td><?php echo count(procesoCompleto($datos,2,'Completo')) ?></td>
                <td><?php echo count(estatusEstado($datos,2,'Acetados')) ?></td>
            </tr>
            <tr class="text-center">
                <th scope="row">Ciencias de la Salud</th>
                <td><?php echo count( Ant_Act_des($datos,10,3)) ?> </td>
                <td><?php echo count( Ant_Act_des($datos,11,3)) ?> </td>
                <td><?php echo count( Ant_Act_des($datos,0,3)) ?> </td>
                <td><?php echo
                        count( Ant_Act_des($datos,10,3)) +
                        count(Ant_Act_des($datos,11,3)) +
                        count(Ant_Act_des($datos,0,3))?>
                </td>
                <td><?php echo count(devueltosEstados($datos,3)) ?></td>
                <td><?php echo count(comiteEstado($datos,3)) ?></td>
                <td><?php echo count(estatusEstado($datos,3,'En revisión')) ?></td>
                <td><?php echo count(estatusEstado($datos,3,'Autores')) ?></td>
                <td><?php echo count(estatusEstado($datos,3,'Rechazado')) ?></td>
                <td><?php echo
                        count(devueltosEstados($datos,3))+
                        count(comiteEstado($datos,3))+
                        count(estatusEstado($datos,3,'En revisión'))+
                        count(estatusEstado($datos,3,'Autores'))+
                        count(estatusEstado($datos,3,'Rechazado'))
                    ?>
                </td>
                <td><?php echo count(procesoCompleto($datos,3,'Completo')) ?></td>
                <td><?php echo count(estatusEstado($datos,3,'Acetados')) ?></td>

            </tr>
            <tr class="text-center">
                <th scope="row">Económico Administrativa</th>
                <td><?php echo count( Ant_Act_des($datos,10,4)) ?> </td>
                <td><?php echo count( Ant_Act_des($datos,11,4)) ?> </td>
                <td><?php echo count( Ant_Act_des($datos,0,4)) ?> </td>
                <td><?php echo
                        count( Ant_Act_des($datos,10,4)) +
                        count(Ant_Act_des($datos,11,4)) +
                        count(Ant_Act_des($datos,0,4))?>
                </td>
                <td><?php echo count(devueltosEstados($datos,4)) ?></td>
                <td><?php echo count(comiteEstado($datos,4)) ?></td>
                <td><?php echo count(estatusEstado($datos,4,'En revisión')) ?></td>
                <td><?php echo count(estatusEstado($datos,4,'Autores')) ?></td>
                <td><?php echo count(estatusEstado($datos,4,'Rechazado')) ?></td>
                <td><?php echo
                        count(devueltosEstados($datos,4))+
                        count(comiteEstado($datos,4))+
                        count(estatusEstado($datos,4,'En revisión'))+
                        count(estatusEstado($datos,4,'Autores'))+
                        count(estatusEstado($datos,4,'Rechazado'))
                    ?>
                </td>
                <td><?php echo count(procesoCompleto($datos,4,'Completo')) ?></td>
                <td><?php echo count(estatusEstado($datos,4,'Acetados')) ?></td>
            </tr>
            <tr class="text-center">
                <th scope="row">Humanidades</th>
                <td><?php echo count( Ant_Act_des($datos,10,5)) ?> </td>
                <td><?php echo count( Ant_Act_des($datos,11,5)) ?> </td>
                <td><?php echo count( Ant_Act_des($datos,0,5)) ?> </td>
                <td><?php echo
                        count( Ant_Act_des($datos,10,5)) +
                        count(Ant_Act_des($datos,11,5)) +
                        count(Ant_Act_des($datos,0,5))?>
                </td>
                <td><?php echo count(devueltosEstados($datos,5)) ?></td>
                <td><?php echo count(comiteEstado($datos,5)) ?></td>
                <td><?php echo count(estatusEstado($datos,5,'En revisión')) ?></td>
                <td><?php echo count(estatusEstado($datos,5,'Autores')) ?></td>
                <td><?php echo count(estatusEstado($datos,5,'Rechazado')) ?></td>
                <td><?php echo
                        count(devueltosEstados($datos,5))+
                        count(comiteEstado($datos,5))+
                        count(estatusEstado($datos,5,'En revisión'))+
                        count(estatusEstado($datos,5,'Autores'))+
                        count(estatusEstado($datos,5,'Rechazado'))
                    ?>
                </td>
                <td><?php echo count(procesoCompleto($datos,5,'Completo')) ?></td>
                <td><?php echo count(estatusEstado($datos,5,'Acetados')) ?></td>
            </tr>
            <tr class="text-center">
                <th scope="row">Técnica</th>
                <td><?php echo count( Ant_Act_des($datos,10,6)) ?> </td>
                <td><?php echo count( Ant_Act_des($datos,11,6)) ?> </td>
                <td><?php echo count( Ant_Act_des($datos,0,6)) ?> </td>
                <td><?php echo
                        count( Ant_Act_des($datos,10,6)) +
                        count(Ant_Act_des($datos,11,6)) +
                        count(Ant_Act_des($datos,0,6))?>
                </td>
                <td><?php echo count(devueltosEstados($datos,6)) ?></td>
                <td><?php echo count(comiteEstado($datos,6)) ?></td>
                <td><?php echo count(estatusEstado($datos,6,'En revisión')) ?></td>
                <td><?php echo count(estatusEstado($datos,6,'Autores')) ?></td>
                <td><?php echo count(estatusEstado($datos,6,'Rechazado')) ?></td>
                <td><?php echo
                        count(devueltosEstados($datos,6))+
                        count(comiteEstado($datos,6))+
                        count(estatusEstado($datos,6,'En revisión'))+
                        count(estatusEstado($datos,6,'Autores'))+
                        count(estatusEstado($datos,6,'Rechazado'))
                    ?>
                </td>
                <td><?php echo count(procesoCompleto($datos,6,'Completo')) ?></td>
                <td><?php echo count(estatusEstado($datos,6,'Acetados')) ?></td>
            </tr>

            </tbody>

            <tfoot>
            <tr class="text-center">
                <th scope="row">Total</th>
                <th scope="row"></th>
                <th scope="row"></th>
                <th scope="row"></th>
                <th scope="row"></th>
                <th scope="row"></th>
                <th scope="row"></th>
                <th scope="row"></th>
                <th scope="row"></th>
                <th scope="row"></th>
                <th scope="row"></th>
                <th scope="row"></th>
                <th scope="row"></th>



            </tr>
            </tfoot>
        </table>
    </div>
</div>
