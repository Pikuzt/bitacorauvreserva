<?php
require_once dirname(__FILE__).'/../core/DB.php';

class mBitacora
{

    public $id;
    public $colaboracion;
    public $id_ojs;
    public $lineamiento;
    public $colaboracionIngles;
    public $colaboracionEspañol;
    public $autores;
    public $correo;
    public $institucion;
    public $dependencia;
    public $comiteEditorial;
    public $fechaRecepcion;
    public $enviadoComite;
    public $fechaEnvioComiteEdi;
    public $asignadoCE;
    public $fechaDictadores;
    public $areaAcademicaFk;
    public $obsfk;
    public $seccionfk;
    public $tipoColfk;
    public $paisfk;
    public $nombreSeccion;
    public $paisFK;
    public $anio;
    public $similitud;
    public $rep_Com_Edi;
    public $num_conv;
    public $status;
    public $proceso;
    public $fRecCorreciones;
    public $fRecCorrecionesSR;
    public $fenvioAutores;
    public $fenvioSGR;
    public $revision1;
    public $revision2;
    public $revision3;
    public $doi;
    public $notas;
    public $fRecCorreciones3;
    public $desConvenio;

    /**
     * @return mixed
     */
    public function getDesConvenio()
    {
        return $this->desConvenio;
    }

    /**
     * @param mixed $desConvenio
     */
    public function setDesConvenio($desConvenio): void
    {
        $this->desConvenio = $desConvenio;
    }

    /**
     * @return mixed
     */
    public function getFRecCorreciones3()
    {
        return $this->fRecCorreciones3;
    }

    /**
     * @param mixed $fRecCorreciones3
     */
    public function setFRecCorreciones3($fRecCorreciones3): void
    {
        $this->fRecCorreciones3 = $fRecCorreciones3;
    }

    /**
     * @return mixed
     */
    public function getFRecCorreciones()
    {
        return $this->fRecCorreciones;
    }

    /**
     * @param mixed $fRecCorreciones
     */
    public function setFRecCorreciones($fRecCorreciones): void
    {
        $this->fRecCorreciones = $fRecCorreciones;
    }

    /**
     * @return mixed
     */
    public function getFRecCorrecionesSR()
    {
        return $this->fRecCorrecionesSR;
    }

    /**
     * @param mixed $fRecCorrecionesSR
     */
    public function setFRecCorrecionesSR($fRecCorrecionesSR): void
    {
        $this->fRecCorrecionesSR = $fRecCorrecionesSR;
    }

    /**
     * @return mixed
     */
    public function getFenvioAutores()
    {
        return $this->fenvioAutores;
    }

    /**
     * @param mixed $fenvioAutores
     */
    public function setFenvioAutores($fenvioAutores): void
    {
        $this->fenvioAutores = $fenvioAutores;
    }

    /**
     * @return mixed
     */
    public function getFenvioSGR()
    {
        return $this->fenvioSGR;
    }

    /**
     * @param mixed $fenvioSGR
     */
    public function setFenvioSGR($fenvioSGR): void
    {
        $this->fenvioSGR = $fenvioSGR;
    }

    /**
     * @return mixed
     */
    public function getRevision1()
    {
        return $this->revision1;
    }

    /**
     * @param mixed $revision1
     */
    public function setRevision1($revision1): void
    {
        $this->revision1 = $revision1;
    }

    /**
     * @return mixed
     */
    public function getRevision2()
    {
        return $this->revision2;
    }

    /**
     * @param mixed $revision2
     */
    public function setRevision2($revision2): void
    {
        $this->revision2 = $revision2;
    }

    /**
     * @return mixed
     */
    public function getRevision3()
    {
        return $this->revision3;
    }

    /**
     * @param mixed $revision3
     */
    public function setRevision3($revision3): void
    {
        $this->revision3 = $revision3;
    }

    /**
     * @return mixed
     */
    public function getDoi()
    {
        return $this->doi;
    }

    /**
     * @param mixed $doi
     */
    public function setDoi($doi): void
    {
        $this->doi = $doi;
    }

    /**
     * @return mixed
     */
    public function getNotas()
    {
        return $this->notas;
    }

    /**
     * @param mixed $notas
     */
    public function setNotas($notas): void
    {
        $this->notas = $notas;
    }




    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getProceso()
    {
        return $this->proceso;
    }

    /**
     * @param mixed $proceso
     */
    public function setProceso($proceso): void
    {
        $this->proceso = $proceso;
    }


    /**
     * @return mixed
     */
    public function getNumConv()
    {
        return $this->num_conv;
    }

    /**
     * @param mixed $num_conv
     */
    public function setNumConv($num_conv): void
    {
        $this->num_conv = $num_conv;
    }

    /**
     * @return mixed
     */
    public function getRepComEdi()
    {
        return $this->rep_Com_Edi;
    }

    /**
     * @param mixed $rep_Com_Edi
     */
    public function setRepComEdi($rep_Com_Edi): void
    {
        $this->rep_Com_Edi = $rep_Com_Edi;
    }

    /**
     * @return mixed
     */
    public function getSimilitud()
    {
        return $this->similitud;
    }

    /**
     * @param mixed $similitud
     */
    public function setSimilitud($similitud): void
    {
        $this->similitud = $similitud;
    }

    /**
     * @return mixed
     */
    public function getAnio()
    {
        return $this->anio;
    }

    /**
     * @param mixed $anio
     */
    public function setAnio($anio): void
    {
        $this->anio = $anio;
    }




    /**
     * @return mixed
     */
    public function getNombreSeccion()
    {
        return $this->nombreSeccion;
    }

    /**
     * @param mixed $nombreSeccion
     */
    public function setNombreSeccion($nombreSeccion): void
    {
        $this->nombreSeccion = $nombreSeccion;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getColaboracion()
    {
        return $this->colaboracion;
    }

    /**
     * @param mixed $colaboracion
     */
    public function setColaboracion($colaboracion)
    {
        $this->colaboracion = $colaboracion;
    }

    /**
     * @return mixed
     */
    public function getIdOjs()
    {
        return $this->id_ojs;
    }

    /**
     * @param mixed $id_ojs
     */
    public function setIdOjs($id_ojs)
    {
        $this->id_ojs = $id_ojs;
    }

    /**
     * @return mixed
     */
    public function getLineamiento()
    {
        return $this->lineamiento;
    }

    /**
     * @param mixed $lineamiento
     */
    public function setLineamiento($lineamiento)
    {
        $this->lineamiento = $lineamiento;
    }

    /**
     * @return mixed
     */
    public function getColaboracionIngles()
    {
        return $this->colaboracionIngles;
    }

    /**
     * @param mixed $colaboracionIngles
     */
    public function setColaboracionIngles($colaboracionIngles)
    {
        $this->colaboracionIngles = $colaboracionIngles;
    }

    /**
     * @return mixed
     */
    public function getColaboracionEspañol()
    {
        return $this->colaboracionEspañol;
    }

    /**
     * @param mixed $colaboracionEspañol
     */
    public function setColaboracionEspañol($colaboracionEspañol)
    {
        $this->colaboracionEspañol = $colaboracionEspañol;
    }

    /**
     * @return mixed
     */
    public function getAutores()
    {
        return $this->autores;
    }

    /**
     * @param mixed $autores
     */
    public function setAutores($autores)
    {
        $this->autores = $autores;
    }

    /**
     * @return mixed
     */
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * @param mixed $correo
     */
    public function setCorreo($correo)
    {
        $this->correo = $correo;
    }

    /**
     * @return mixed
     */
    public function getInstitucion()
    {
        return $this->institucion;
    }

    /**
     * @param mixed $institucion
     */
    public function setInstitucion($institucion)
    {
        $this->institucion = $institucion;
    }

    /**
     * @return mixed
     */
    public function getDependencia()
    {
        return $this->dependencia;
    }

    /**
     * @param mixed $dependencia
     */
    public function setDependencia($dependencia)
    {
        $this->dependencia = $dependencia;
    }

    /**
     * @return mixed
     */
    public function getComiteEditorial()
    {
        return $this->comiteEditorial;
    }

    /**
     * @param mixed $comiteEditorial
     */
    public function setComiteEditorial($comiteEditorial)
    {
        $this->comiteEditorial = $comiteEditorial;
    }

    /**
     * @return mixed
     */
    public function getFechaRecepcion()
    {
        return $this->fechaRecepcion;
    }

    /**
     * @param mixed $fechaRecepcion
     */
    public function setFechaRecepcion($fechaRecepcion)
    {
        $this->fechaRecepcion = $fechaRecepcion;
    }

    /**
     * @return mixed
     */
    public function getEnviadoComite()
    {
        return $this->enviadoComite;
    }

    /**
     * @param mixed $enviadoComite
     */
    public function setEnviadoComite($enviadoComite)
    {
        $this->enviadoComite = $enviadoComite;
    }

    /**
     * @return mixed
     */
    public function getFechaEnvioComiteEdi()
    {
        return $this->fechaEnvioComiteEdi;
    }

    /**
     * @param mixed $fechaEnvioComiteEdi
     */
    public function setFechaEnvioComiteEdi($fechaEnvioComiteEdi)
    {
        $this->fechaEnvioComiteEdi = $fechaEnvioComiteEdi;
    }

    /**
     * @return mixed
     */
    public function getAsignadoCE()
    {
        return $this->asignadoCE;
    }

    /**
     * @param mixed $asignadoCE
     */
    public function setAsignadoCE($asignadoCE)
    {
        $this->asignadoCE = $asignadoCE;
    }

    /**
     * @return mixed
     */
    public function getFechaDictadores()
    {
        return $this->fechaDictadores;
    }

    /**
     * @param mixed $fechaDictadores
     */
    public function setFechaDictadores($fechaDictadores)
    {
        $this->fechaDictadores = $fechaDictadores;
    }

    /**
     * @return mixed
     */
    public function getAreaAcademicaFk()
    {
        return $this->areaAcademicaFk;
    }

    /**
     * @param mixed $areaAcademicaFk
     */
    public function setAreaAcademicaFk($areaAcademicaFk)
    {
        $this->areaAcademicaFk = $areaAcademicaFk;
    }

    /**
     * @return mixed
     */
    public function getObsfk()
    {
        return $this->obsfk;
    }

    /**
     * @param mixed $obsfk
     */
    public function setObsfk($obsfk)
    {
        $this->obsfk = $obsfk;
    }

    /**
     * @return mixed
     */
    public function getSeccionfk()
    {
        return $this->seccionfk;
    }

    /**
     * @param mixed $seccionfk
     */
    public function setSeccionfk($seccionfk)
    {
        $this->seccionfk = $seccionfk;
    }

    /**
     * @return mixed
     */
    public function getTipoColfk()
    {
        return $this->tipoColfk;
    }

    /**
     * @param mixed $tipoColfk
     */
    public function setTipoColfk($tipoColfk)
    {
        $this->tipoColfk = $tipoColfk;
    }

    /**
     * @return mixed
     */
    public function getPaisfk()
    {
        return $this->paisfk;
    }

    /**
     * @param mixed $paisfk
     */
    public function setPaisfk($paisfk)
    {
        $this->paisfk = $paisfk;
    }

    public function paises (){
        $conexion = new DB();
        $conn = $conexion->connection();
        $sql = "select * from paises;";

        $puntos = $conn->prepare($sql);
        $puntos->execute();
        $result = $puntos->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }

    public function seccion(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $sql = "select * from cuo_bit_uvservat_seccion;";

        $puntos = $conn->prepare($sql);
        $puntos->execute();
        $result = $puntos->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }

    public function colabora(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $sql = "select * from cuo_bit_tipo_colaboracion;";

        $puntos = $conn->prepare($sql);
        $puntos->execute();
        $result = $puntos->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }

    public function observatorio(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $sql = "select * from cuo_uvserva_observatorio;";

        $puntos = $conn->prepare($sql);
        $puntos->execute();
        $result = $puntos->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }

    public function areaAcademcia(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $sql = "select * from cuo_uvserva_area_academina;";

        $puntos = $conn->prepare($sql);
        $puntos->execute();
        $result = $puntos->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }


    public function addSeccion(){

        $nameSeccion = $this->getNombreSeccion();

        $conexion = new DB();
        $conn = $conexion->connection();
        $sql = "insert into cuo_bit_uvservat_seccion (nombre_sec) values (?);";

        $query = $conn->prepare($sql);
        $query->bindParam(1,$nameSeccion);
        $result = $query->execute();
        return $result;
    }

    public function editSeccion(){

        $nameSeccion = $this->getNombreSeccion();
        $idSeccion = $this->getId();


        $conexion = new DB();
        $conn = $conexion->connection();
        $sql = "update  cuo_bit_uvservat_seccion set
                nombre_sec = :nombre
                where id_seccion = :id";
        $query = $conn->prepare($sql);
        $query->bindParam(':nombre',$nameSeccion);
        $query->bindParam(':id',$idSeccion);
        $result = $query->execute();
        return $result;
    }

    public function dataSeccion(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $sql = "select * from cuo_bit_uvservat_seccion";
        $puntos = $conn->prepare($sql);
        $puntos->execute();
        $result = $puntos->fetchAll(PDO::FETCH_OBJ);
        return $result;

    }

    public function dataSeccionRepea(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $seccion = $this->getNombreSeccion();
        $sql = "select * from cuo_bit_uvservat_seccion where nombre_sec = ?";
        $query = $conn->prepare($sql);
        $query->bindParam(1,$seccion);
        $query->execute();
        $result = $query->fetchColumn();

        return $result;

    }
    public function dataOb(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $sql = "select * from cuo_uvserva_observatorio";
        $puntos = $conn->prepare($sql);
        $puntos->execute();
        $result = $puntos->fetchAll(PDO::FETCH_OBJ);
        return $result;

    }

    public function addObservatorio(){

        $nameSeccion = $this->getNombreSeccion();

        $conexion = new DB();
        $conn = $conexion->connection();
        $sql = "insert into cuo_uvserva_observatorio (nombre_observa) values (?);";
        $query = $conn->prepare($sql);
        $query->bindParam(1,$nameSeccion);
        $result = $query->execute();
        return $result;
    }

    public function editObservatorio(){

        $nameObs = $this->getNombreSeccion();
        $id = $this->getId();


        $conexion = new DB();
        $conn = $conexion->connection();
        $sql = "update  cuo_uvserva_observatorio set
                nombre_observa = :nombre
                where id_observatorio = :id";
        $query = $conn->prepare($sql);
        $query->bindParam(':nombre',$nameObs);
        $query->bindParam(':id',$id);
        $result = $query->execute();
        return $result;
    }

    public function repetidoObs(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $seccion = $this->getNombreSeccion();
        $sql = "select * from cuo_uvserva_observatorio where nombre_observa = ?";
        $query = $conn->prepare($sql);
        $query->bindParam(1,$seccion);
        $query->execute();
        $result = $query->fetchColumn();
        return $result;

    }



    public function dataTipo(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $sql = "select * from cuo_bit_tipo_colaboracion";
        $puntos = $conn->prepare($sql);
        $puntos->execute();
        $result = $puntos->fetchAll(PDO::FETCH_OBJ);
        return $result;

    }

    public function addTC(){
        $nameSeccion = $this->getNombreSeccion();
        $conexion = new DB();
        $conn = $conexion->connection();
        $sql = "insert into cuo_bit_tipo_colaboracion (nombre_colaboracion) values (?);";
        $query = $conn->prepare($sql);
        $query->bindParam(1,$nameSeccion);
        $result = $query->execute();
        return $result;
    }

    public function repetidoTC(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $seccion = $this->getNombreSeccion();
        $sql = "select * from cuo_bit_tipo_colaboracion where nombre_colaboracion = ?";
        $query = $conn->prepare($sql);
        $query->bindParam(1,$seccion);
        $query->execute();
        $result = $query->fetchColumn();
        return $result;

    }

    public function editTC(){

        $nameObs = $this->getNombreSeccion();
        $id = $this->getId();


        $conexion = new DB();
        $conn = $conexion->connection();
        $sql = "update  cuo_bit_tipo_colaboracion set
                nombre_colaboracion = :nombre
                where id_tipo_Colaboracion = :id";
        $query = $conn->prepare($sql);
        $query->bindParam(':nombre',$nameObs);
        $query->bindParam(':id',$id);
        $result = $query->execute();
        return $result;
    }


    public function dataAA(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $sql = "select * from cuo_uvserva_area_academina";
        $puntos = $conn->prepare($sql);
        $puntos->execute();
        $result = $puntos->fetchAll(PDO::FETCH_OBJ);
        return $result;

    }

    public function addAA(){
        $nameSeccion = $this->getNombreSeccion();
        $conexion = new DB();
        $conn = $conexion->connection();
        $sql = "insert into cuo_uvserva_area_academina (nombreArea) values (?);";
        $query = $conn->prepare($sql);
        $query->bindParam(1,$nameSeccion);
        $result = $query->execute();
        return $result;
    }

    public function editAA(){

        $nameObs = $this->getNombreSeccion();
        $id = $this->getId();

        $conexion = new DB();
        $conn = $conexion->connection();
        $sql = "update  cuo_uvserva_area_academina set
                nombreArea = :nombre
                where id_area_academica = :id";
        $query = $conn->prepare($sql);
        $query->bindParam(':nombre',$nameObs);
        $query->bindParam(':id',$id);
        $result = $query->execute();
        return $result;
    }

    public function repetidoAA(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $seccion = $this->getNombreSeccion();

        $sql = "select * from cuo_uvserva_area_academina where nombreArea like ?";

        //var_dump($sql);
        $query = $conn->prepare($sql);
        $query->bindParam(1,$seccion);
        $query->execute();
        $result = $query->fetchColumn();
        return $result;

    }


    public function addArticulo(){

            $nc = $this->getColaboracion();
            $ojs = $this->getIdOjs();
            $lineamiento = $this->getLineamiento();
            $autor = $this->getAutores();
            $correo = $this->getCorreo();
            $institucion = $this->getInstitucion();
            $dependencia = $this->getDependencia();
            $envioCom = $this->getEnviadoComite();
            $fecEnvCom = $this->getFechaEnvioComiteEdi();
            $asignadoCE = $this->getAsignadoCE();
            $fechaDic = $this->getFechaDictadores();
            $arefk = $this->getAreaAcademicaFk();
            $obsfk = $this->getObsfk();
            $seccionfk = $this->getSeccionfk();
            $tcfk = $this->getTipoColfk();
            $paisfk = $this->getPaisfk();
            $anio = $this->getAnio();
            $similitud = $this->getSimilitud();
            $re_Com_Edi = $this->getComiteEditorial();
            $numC = $this->getNumConv();
            $colaboracionEspañol= $this->getColaboracionEspañol();
            $colaboracionIngles= $this->getColaboracionIngles();
            $fechaRec = $this->getFechaRecepcion();
            $status = $this->getStatus();
            $proceso = $this->getProceso();
            $fecha_en_Au = $this->getFenvioAutores();
            $fecha_recep_corre = $this->getFRecCorreciones();
            $fecha_en_SR= $this->getFRecCorrecionesSR();
            $revisor1= $this->getRevision1();
            $revisor2= $this->getRevision2();
            $revisor3= $this->getRevision3();
            $fecha_recep_corre2 = $this->getFRecCorreciones3();
            $doi = $this->getDoi();
            $nota = $this->getNotas();
            $desConv = $this->getDesConvenio();







            $conexion = new DB();
            $conn = $conexion->connection();
            $conn->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );
            $sql = "insert into 
                cuo_bitacora_uvserva_bitacora (     
                                               num_colaboracion,
                                               id_envio_ojs,
                                               anio,
                                               conv_num, 
                                               id_pais_fk,
                                               similitud,
                                               lineamiento,
                                               Rep_comite_editorial,
                                               institucion,
                                               colaboracionIngles,
                                               colaboracionEspañol,
                                               autores,
                                               correo,
                                               dependencia_centroTrabajo,
                                               id_area_cademica_fk,
                                               id_obs_fk,
                                               id_seccion_fk,
                                               id_tipo_col_fk,
                                               fecha_recepcion,
                                               enviado_comite,
                                               fecha_envio_comiteEdi,
                                               Asignado_CE,
                                               fecha_asig_dictadores,
                                               status,
                                               proceso,
                                               fecha_en_Au,
                                               fecha_recep_corre,
                                               fecha_en_SR,
                                               revisor1,
                                               revisor2,
                                               revisor3,
                                               fecha_recep_corre2,
                                               doi,
                                               nota,
                                               desconvenio                                               
                                               )values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?
                                               ,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            $query = $conn->prepare($sql);
            $query->bindParam(1,$nc);
            $query->bindParam(2,$ojs);
            $query->bindParam(3,$anio);
            $query->bindParam(4,$numC);
            $query->bindParam(5,$paisfk);
            $query->bindParam(6,$similitud);
            $query->bindParam(7,$lineamiento);
            $query->bindParam(8,$re_Com_Edi);
            $query->bindParam(9,$institucion);
            $query->bindParam(10,$colaboracionIngles);
            $query->bindParam(11,$colaboracionEspañol);
            $query->bindParam(12,$autor);
            $query->bindParam(13,$correo);
            $query->bindParam(14,$dependencia);
            $query->bindParam(15,$arefk);
            $query->bindParam(16,$obsfk);
            $query->bindParam(17,$seccionfk);
            $query->bindParam(18,$tcfk);
            $query->bindParam(19,$fechaRec);
            $query->bindParam(20,$envioCom);
            $query->bindParam(21,$fecEnvCom);
            $query->bindParam(22,$asignadoCE);
            $query->bindParam(23,$fechaDic);
            $query->bindParam(24,$status);
            $query->bindParam(25,$proceso);
            $query->bindParam(26,$fecha_en_Au);
            $query->bindParam(27,$fecha_recep_corre);
            $query->bindParam(28,$fecha_en_SR);
            $query->bindParam(29,$revisor1);
            $query->bindParam(30,$revisor2);
            $query->bindParam(31,$revisor3);
            $query->bindParam(32,$fecha_recep_corre2);
            $query->bindParam(33,$doi);
            $query->bindParam(34,$nota);
            $query->bindParam(35,$desConv);
            $result= $query->execute();
            return $result;

    }

    public function EditArticulo(){

        $nc = $this->getColaboracion();
        $ojs = $this->getIdOjs();
        $lineamiento = $this->getLineamiento();
        $autor = $this->getAutores();
        $correo = $this->getCorreo();
        $institucion = $this->getInstitucion();
        $dependencia = $this->getDependencia();
        $arefk = $this->getAreaAcademicaFk();
        $obsfk = $this->getObsfk();
        $seccionfk = $this->getSeccionfk();
        $tcfk = $this->getTipoColfk();
        $paisfk = $this->getPaisfk();
        $anio = $this->getAnio();
        $similitud = $this->getSimilitud();
        $re_Com_Edi = $this->getComiteEditorial();
        $numC = $this->getNumConv();
        $colaboracionEspañol= $this->getColaboracionEspañol();
        $colaboracionIngles= $this->getColaboracionIngles();
        $id = $this->getId();
        $fechaRec = $this->getFechaRecepcion();
        $envioCom = $this->getEnviadoComite();
        $fecEnvCom = $this->getFechaEnvioComiteEdi();
        $asignadoCE = $this->getAsignadoCE();
        $fechaDic = $this->getFechaDictadores();
        $status = $this->getStatus();
        $proceso = $this->getProceso();
        $fecha_en_Au = $this->getFenvioAutores();
        $fecha_recep_corre = $this->getFRecCorreciones();
        $fecha_en_SR= $this->getFRecCorrecionesSR();
        $revisor1= $this->getRevision1();
        $revisor2= $this->getRevision2();
        $revisor3= $this->getRevision3();
        $fecha_recep_corre2 = $this->getFRecCorreciones3();
        $doi = $this->getDoi();
        $nota = $this->getNotas();
        $desConv = $this->getDesConvenio();







        $conexion = new DB();
        $conn = $conexion->connection();
        $conn->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );
        $sql = "update  cuo_bitacora_uvserva_bitacora set                
                num_colaboracion = ?,
                id_envio_ojs = ?,
                anio = ?,
                conv_num = ?,
                id_pais_fk = ?,
                similitud = ?,
                lineamiento = ?,
                Rep_comite_editorial = ?,
                institucion = ?,
                colaboracionIngles = ?,
                colaboracionEspañol = ?,
                autores = ?,
                correo = ?,
                dependencia_centroTrabajo = ?,
                id_area_cademica_fk = ?,
                id_obs_fk = ?,
                id_seccion_fk = ?,
                id_tipo_col_fk = ?,
                fecha_recepcion= ?,
                enviado_comite=?,
                fecha_envio_comiteEdi=?,
                Asignado_CE=?,
                fecha_asig_dictadores=?,
                status=?,
                proceso=?,
                fecha_en_Au=?,
                fecha_recep_corre=?,
                fecha_en_SR=?,
                revisor1=?,                                  
                revisor2=?,                                  
                revisor3=?,                                  
                fecha_recep_corre2=?,                                  
                doi=?,                                  
                nota=?,                                  
                desconvenio=?                                  
                where id_bitacora = ?";
        $query = $conn->prepare($sql);
        $query->bindParam(1,$nc);
        $query->bindParam(2,$ojs);
        $query->bindParam(3,$anio);
        $query->bindParam(4,$numC);
        $query->bindParam(5,$paisfk);
        $query->bindParam(6,$similitud);
        $query->bindParam(7,$lineamiento);
        $query->bindParam(8,$re_Com_Edi);
        $query->bindParam(9,$institucion);
        $query->bindParam(10,$colaboracionIngles);
        $query->bindParam(11,$colaboracionEspañol);
        $query->bindParam(12,$autor);
        $query->bindParam(13,$correo);
        $query->bindParam(14,$dependencia);
        $query->bindParam(15,$arefk);
        $query->bindParam(16,$obsfk);
        $query->bindParam(17,$seccionfk);
        $query->bindParam(18,$tcfk);
        $query->bindParam(19,$fechaRec);
        $query->bindParam(20,$envioCom);
        $query->bindParam(21,$fecEnvCom);
        $query->bindParam(22,$asignadoCE);
        $query->bindParam(23,$fechaDic);
        $query->bindParam(24,$status);
        $query->bindParam(25,$proceso);
        $query->bindParam(26,$fecha_en_Au);
        $query->bindParam(27,$fecha_recep_corre);
        $query->bindParam(28,$fecha_en_SR);
        $query->bindParam(29,$revisor1);
        $query->bindParam(30,$revisor2);
        $query->bindParam(31,$revisor3);
        $query->bindParam(32,$fecha_recep_corre2);
        $query->bindParam(33,$doi);
        $query->bindParam(34,$nota);
        $query->bindParam(35,$desConv);
        $query->bindParam(36,$id);
        $result= $query->execute();
        return $result;

    }

    public function dataArticulo(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $sql = "
        
       select *            
from cuo_bitacora_uvserva_bitacora
inner join cuo_bit_tipo_colaboracion cbtc on cuo_bitacora_uvserva_bitacora.id_tipo_col_fk = cbtc.id_tipo_Colaboracion
inner join cuo_bit_uvservat_seccion cbus on cuo_bitacora_uvserva_bitacora.id_seccion_fk = cbus.id_seccion
inner join cuo_uvserva_area_academina cuaa on cuo_bitacora_uvserva_bitacora.id_area_cademica_fk = cuaa.id_area_academica
inner join cuo_uvserva_observatorio cuo on cuo_bitacora_uvserva_bitacora.id_obs_fk = cuo.id_observatorio
inner join paises p on cuo_bitacora_uvserva_bitacora.id_pais_fk = p.id;
        ";
        $puntos = $conn->prepare($sql);
        $puntos->execute();
        $result = $puntos->fetchAll(PDO::FETCH_OBJ);
        return $result;

    }

    public function DataEditArt(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $id = $this->getId();
        $sql = "        
        select *
from cuo_bitacora_uvserva_bitacora
inner join cuo_bit_tipo_colaboracion cbtc on cuo_bitacora_uvserva_bitacora.id_tipo_col_fk = cbtc.id_tipo_Colaboracion
inner join cuo_bit_uvservat_seccion cbus on cuo_bitacora_uvserva_bitacora.id_seccion_fk = cbus.id_seccion
inner join cuo_uvserva_area_academina cuaa on cuo_bitacora_uvserva_bitacora.id_area_cademica_fk = cuaa.id_area_academica
inner join cuo_uvserva_observatorio cuo on cuo_bitacora_uvserva_bitacora.id_obs_fk = cuo.id_observatorio
inner join paises p on cuo_bitacora_uvserva_bitacora.id_pais_fk = p.id
where id_bitacora = '$id';
        ";
        $query = $conn->prepare($sql);
        $query ->execute();
        $result = $query->fetchAll(PDO::FETCH_OBJ);
        return $result;
        }

        public function DataRevision(){

                $conexion = new DB();
                    $id = $this->getId();
                $conn = $conexion->connection();
                $sql = "select * from cuo_uvserva_revision where id_art_fk ='$id' ";
                $query = $conn->prepare($sql);
                $query ->execute();
                $result = $query->fetchAll(PDO::FETCH_OBJ);
                return $result;

        }

        public function addRevision(){
        $id = $this->getId();
        $nombrev = $this->getPaisfk();
        $dictamenFechav = $this->getComiteEditorial();
        $fRecepcionV = $this->getColaboracionEspañol();
        $recomendacionRV = $this->getColaboracionIngles();
        $contanciaEV = $this->getAutores();




            $conexion = new DB();
            $conn = $conexion->connection();
            $sql = "insert into cuo_uvserva_revision 
                    (nombre_revision,
                     fecha_envio,
                     fecha_recepcion,
                     recomendacion,
                     constancia_enviada,
                     id_art_fk                     
                     ) 
                        values 
                    (?,?,?,?,?,?)";

            $query = $conn->prepare($sql);
            $query->bindParam(1,$nombrev);
            $query->bindParam(2,$dictamenFechav);
            $query->bindParam(3,$fRecepcionV);
            $query->bindParam(4,$recomendacionRV);
            $query->bindParam(5,$contanciaEV);
            $query->bindParam(6,$id);
            $result = $query->execute();
            return $result;


        }

        public function EditRevision(){

        $id = $this->getId();
        $nombrev = $this->getPaisfk();
        $dictamenFechav = $this->getComiteEditorial();
        $fRecepcionV = $this->getColaboracionEspañol();
        $recomendacionRV = $this->getColaboracionIngles();
        $contanciaEV = $this->getAutores();



        $conexion = new DB();
        $conn = $conexion->connection();
        $sql = "update  cuo_uvserva_revision set                
                nombre_revision = :nombre_revision,
                fecha_envio = :fecha_envio,
                fecha_recepcion = :fecha_recepcion,
                recomendacion = :recomendacion,
                constancia_enviada = :constancia_enviada
                where id_revision = :id_revision";
        $query = $conn->prepare($sql);
        $query->bindParam(':nombre_revision',$nombrev);
        $query->bindParam(':fecha_envio',$fRecepcionV);
        $query->bindParam(':fecha_recepcion',$dictamenFechav);
        $query->bindParam(':recomendacion',$recomendacionRV);
        $query->bindParam(':constancia_enviada',$contanciaEV);
        $query->bindParam(':id_revision',$id);
        $result = $query->execute();
        return $result;
    }

    public function DataRevisionEdit(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $id = $this->getId();
        $sql = "        
        select * from cuo_uvserva_revision where id_revision ='$id'
        ";
        $query = $conn->prepare($sql);
        $query ->execute();
        $result = $query->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }

    public function tablaAvances(){


        $conexion = new DB();
        $num = $this->getId();
        $conn = $conexion->connection();

        $sql = "        
        
    select
    count(case when cuaa.nombreArea = 'Artes' and cbub.conv_num = 10   then 1 end ) as Con_Anteriores_Artes,
    count(case when cuaa.nombreArea = 'Biológico Agropecuaria' and cbub.conv_num = 10  then 1 end ) as Con_Anteriores_Biológico_Agropecuaria,
    count(case when cuaa.nombreArea = 'Ciencias de la Salud' and cbub.conv_num = 10  then 1 end ) as Con_Anteriores_Ciencias_Salud,
    count(case when cuaa.nombreArea = 'Económico Administrativa' and cbub.conv_num = 10  then 1 end ) as Con_Anteriores_Económico_Administrativa,
    count(case when cuaa.nombreArea = 'Humanidades' and cbub.conv_num = 10  then 1 end ) as Con_Anteriores_Humanidades,
    count(case when cuaa.nombreArea = 'Técnica' and cbub.conv_num = 10  then 1 end ) as Con_Anteriores_Técnica,
    count(case when cuaa.nombreArea = 'Artes' and cbub.conv_num = 11  then 1 end ) as Con_Actuales_Artes,
    count(case when cuaa.nombreArea = 'Biológico Agropecuaria' and cbub.conv_num = 11  then 1 end ) as Con_Actuales_Biológico_Agropecuaria,
    count(case when cuaa.nombreArea = 'Ciencias de la Salud' and cbub.conv_num = 11  then 1 end ) as Con_Actuales_Ciencias_Salud,
    count(case when cuaa.nombreArea = 'Económico Administrativa' and cbub.conv_num = 11  then 1 end ) as Con_Actuales_Económico_Administrativa,
    count(case when cuaa.nombreArea = 'Humanidades' and cbub.conv_num = 11  then 1 end ) as Con_Actuales_Humanidades,
    count(case when cuaa.nombreArea = 'Técnica' and cbub.conv_num = 0  then 1 end ) as Con_Actuales_Técnica,
    count(case when cuaa.nombreArea = 'Artes' and cbub.conv_num = 0  then 1 end ) as Con_Despues_Artes,
    count(case when cuaa.nombreArea = 'Biológico Agropecuaria' and cbub.conv_num = 0  then 1 end ) as Con_Despues_Biológico_Agropecuaria,
    count(case when cuaa.nombreArea = 'Ciencias de la Salud' and cbub.conv_num = 0  then 1 end ) as Con_Despues_Ciencias_Salud,
    count(case when cuaa.nombreArea = 'Económico Administrativa' and cbub.conv_num = 0  then 1 end ) as Con_Despues_Económico_Administrativa,
    count(case when cuaa.nombreArea = 'Humanidades' and cbub.conv_num = 0  then 1 end ) as Con_Despues_Humanidades,
    count(case when cuaa.nombreArea = 'Técnica' and cbub.conv_num = 0  then 1 end ) as Con_Despues_Técnica,
    count(case when cuaa.nombreArea = 'Artes' and cbub.lineamiento = 'no'  then 1 end ) as total_lineamiento_Artes,
    count(case when cuaa.nombreArea = 'Biológico Agropecuaria' and cbub.lineamiento = 'no'   then 1 end ) as total_lineamiento_BA,
    count(case when cuaa.nombreArea = 'Ciencias de la Salud' and cbub.lineamiento = 'no'   then 1 end ) as total_lineamiento_CS,
    count(case when cuaa.nombreArea = 'Económico Administrativa' and cbub.lineamiento = 'no'   then 1 end ) as total_lineamiento_EA,
    count(case when cuaa.nombreArea = 'Humanidades' and cbub.lineamiento = 'no'   then 1 end ) as total_lineamiento_H,
    count(case when cuaa.nombreArea = 'Técnica' and cbub.lineamiento = 'no'   then 1 end ) as total_lineamiento_T,
    count(case when cuaa.nombreArea = 'Artes' and cbub.enviado_comite = 'si'   then 1 end ) as EV_Artes,
    count(case when cuaa.nombreArea = 'Biológico Agropecuaria' and cbub.enviado_comite = 'si'   then 1 end ) as EV_BA,
    count(case when cuaa.nombreArea = 'Económico Administrativa' and cbub.enviado_comite = 'si'   then 1 end ) as EV_EA,
    count(case when cuaa.nombreArea = 'Humanidades' and cbub.enviado_comite = 'si'   then 1 end ) as EV_H,
    count(case when cuaa.nombreArea = 'Técnica' and cbub.enviado_comite = 'si'   then 1 end ) as EV_T
    from cuo_bitacora_uvserva_bitacora cbub
    inner join cuo_uvserva_area_academina cuaa on cbub.id_area_cademica_fk = cuaa.id_area_academica;
        
        ";
        $query = $conn->prepare($sql);
        $query ->execute();
        $result = $query->fetchAll(PDO::FETCH_OBJ);

        $conexion->closeConnecion();
        return $result;
    }

    public function actuales(){
        $conexion = new DB();
        $id = $this->getId();
        $conn = $conexion->connection();
        $sql = "        
        
  
select
    count(case when cuaa.nombreArea = 'Artes' and cbub.conv_num = 11   then 1 end ) as Actuales_A,
    count(case when cuaa.nombreArea = 'Biológico Agropecuaria' and cbub.conv_num = 11  then 1 end ) as Actuales_BA,
    count(case when cuaa.nombreArea = 'Ciencias de la Salud' and cbub.conv_num = 11  then 1 end ) as Actuales_CS,
    count(case when cuaa.nombreArea = 'Económico Administrativa' and cbub.conv_num = 11  then 1 end ) as Actuales_EA,
    count(case when cuaa.nombreArea = 'Humanidades' and cbub.conv_num = 11  then 1 end ) as Actuales_H,
    count(case when cuaa.nombreArea = 'Técnica' and cbub.conv_num = 11  then 1 end ) as Actuales_T
from cuo_bitacora_uvserva_bitacora cbub
    inner join cuo_uvserva_area_academina cuaa on cbub.id_area_cademica_fk = cuaa.id_area_academica;
        
        ";
        $query = $conn->prepare($sql);
        $query ->execute();
        $result = $query->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }

    public function anteriores(){
        $conexion = new DB();
        $id = $this->getId();
        $conn = $conexion->connection();
        $sql = "        
        
  
select
    count(case when cuaa.nombreArea = 'Artes' and cbub.conv_num = 10   then 1 end ) as Anteriores_A,
    count(case when cuaa.nombreArea = 'Biológico Agropecuaria' and cbub.conv_num = 10  then 1 end ) as Anteriores_BA,
    count(case when cuaa.nombreArea = 'Ciencias de la Salud' and cbub.conv_num = 10  then 1 end ) as Anteriores_CS,
    count(case when cuaa.nombreArea = 'Económico Administrativa' and cbub.conv_num = 10  then 1 end ) as Anteriores_EA,
    count(case when cuaa.nombreArea = 'Humanidades' and cbub.conv_num = 10  then 1 end ) as Anteriores_H,
    count(case when cuaa.nombreArea = 'Técnica' and cbub.conv_num = 10  then 1 end ) as Anteriores_T
from cuo_bitacora_uvserva_bitacora cbub
    inner join cuo_uvserva_area_academina cuaa on cbub.id_area_cademica_fk = cuaa.id_area_academica;
        
        ";
        $query = $conn->prepare($sql);
        $query ->execute();
        $result = $query->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }

    public function Despues(){
        $conexion = new DB();
        $id = $this->getId();
        $conn = $conexion->connection();
        $sql = "        
        
  
select
    count(case when cuaa.nombreArea = 'Artes' and cbub.conv_num = 0   then 1 end ) as Despues_A,
    count(case when cuaa.nombreArea = 'Biológico Agropecuaria' and cbub.conv_num = 0  then 1 end ) as Despues_BA,
    count(case when cuaa.nombreArea = 'Ciencias de la Salud' and cbub.conv_num = 0  then 1 end ) as Despues_CS,
    count(case when cuaa.nombreArea = 'Económico Administrativa' and cbub.conv_num = 0  then 1 end ) as Despues_EA,
    count(case when cuaa.nombreArea = 'Humanidades' and cbub.conv_num = 0  then 1 end ) as Despues_H,
    count(case when cuaa.nombreArea = 'Técnica' and cbub.conv_num = 0  then 1 end ) as Despues_T
from cuo_bitacora_uvserva_bitacora cbub
    inner join cuo_uvserva_area_academina cuaa on cbub.id_area_cademica_fk = cuaa.id_area_academica;
        
        ";
        $query = $conn->prepare($sql);
        $query ->execute();
        $result = $query->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }


    public function Total(){
        $conexion = new DB();
        $id = $this->getId();
        $conn = $conexion->connection();
        $sql = "        
        
  select
count(case when cuaa.nombreArea = 'Artes' and cbub.lineamiento = 'no'  then 1 end ) as total_A,
    count(case when cuaa.nombreArea = 'Biológico Agropecuaria' and cbub.lineamiento = 'no'   then 1 end ) as total_BA,
    count(case when cuaa.nombreArea = 'Ciencias de la Salud' and cbub.lineamiento = 'no'   then 1 end ) as total_CS,
    count(case when cuaa.nombreArea = 'Económico Administrativa' and cbub.lineamiento = 'no'   then 1 end ) as total_EA,
    count(case when cuaa.nombreArea = 'Humanidades' and cbub.lineamiento = 'no'   then 1 end ) as total_H,
    count(case when cuaa.nombreArea = 'Técnica' and cbub.lineamiento = 'no'   then 1 end ) as total_T
        from cuo_bitacora_uvserva_bitacora cbub
    inner join cuo_uvserva_area_academina cuaa on cbub.id_area_cademica_fk = cuaa.id_area_academica;
        ";
        $query = $conn->prepare($sql);
        $query ->execute();
        $result = $query->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }

    public function statusRevision(){
        $conexion = new DB();
        $id = $this->getId();
        $conn = $conexion->connection();
        $sql = "    
    select
    count(case when cuaa.nombreArea = 'Artes' and cbub.status = 'En revisión'  then 1 end ) as Status_Re_A,
    count(case when cuaa.nombreArea = 'Biológico Agropecuaria' and cbub.status = 'En revisión'  then 1 end ) as Status_Re_BA,
    count(case when cuaa.nombreArea = 'Ciencias de la Salud' and cbub.status = 'En revisión'  then 1 end ) as Status_Re_CS,
    count(case when cuaa.nombreArea = 'Económico Administrativa' and cbub.status = 'En revisión'  then 1 end ) as Status_Re_EA,
    count(case when cuaa.nombreArea = 'Humanidades' and cbub.status = 'En revisión'  then 1 end ) as Status_Re_H,
    count(case when cuaa.nombreArea = 'Técnica' and cbub.status = 'En revisión'  then 1 end ) as Status_Re_T
   from cuo_bitacora_uvserva_bitacora cbub
    inner join cuo_uvserva_area_academina cuaa on cbub.id_area_cademica_fk = cuaa.id_area_academica;
        ";
        $query = $conn->prepare($sql);
        $query ->execute();
        $result = $query->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }

    public function EnvComite(){
        $conexion = new DB();
        $id = $this->getId();
        $conn = $conexion->connection();
        $sql = "    
    select
    count(case when cuaa.nombreArea = 'Artes' and cbub.enviado_comite = 'si'  then 1 end ) as Comi_env_A,
    count(case when cuaa.nombreArea = 'Biológico Agropecuaria' and cbub.enviado_comite = 'si'  then 1 end ) as Comi_env_BA,
    count(case when cuaa.nombreArea = 'Ciencias de la Salud' and cbub.enviado_comite = 'si'  then 1 end ) as Comi_env_CS,
    count(case when cuaa.nombreArea = 'Económico Administrativa' and cbub.enviado_comite = 'si'  then 1 end ) as Comi_env_EA,
    count(case when cuaa.nombreArea = 'Humanidades' and cbub.enviado_comite = 'si'  then 1 end ) as Comi_env_H,
    count(case when cuaa.nombreArea = 'Técnica' and cbub.enviado_comite = 'si'  then 1 end ) as Comi_env_T
   from cuo_bitacora_uvserva_bitacora cbub
    inner join cuo_uvserva_area_academina cuaa on cbub.id_area_cademica_fk = cuaa.id_area_academica;
        ";
        $query = $conn->prepare($sql);
        $query ->execute();
        $result = $query->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }


    public function statusCorrecion(){
        $conexion = new DB();
        $id = $this->getId();
        $conn = $conexion->connection();
        $sql = "    
    select
    count(case when cuaa.nombreArea = 'Artes' and cbub.status = 'Autores'  then 1 end ) as Status_A_A,
    count(case when cuaa.nombreArea = 'Biológico Agropecuaria' and cbub.status = 'Autores'  then 1 end ) as Status_A_BA,
    count(case when cuaa.nombreArea = 'Ciencias de la Salud' and cbub.status = 'Autores'  then 1 end ) as Status_A_CS,
    count(case when cuaa.nombreArea = 'Económico Administrativa' and cbub.status = 'Autores'  then 1 end ) as Status_A_EA,
    count(case when cuaa.nombreArea = 'Humanidades' and cbub.status = 'Autores'  then 1 end ) as Status_A_H,
    count(case when cuaa.nombreArea = 'Técnica' and cbub.status = 'Autores'  then 1 end ) as Status_A_T
   from cuo_bitacora_uvserva_bitacora cbub
    inner join cuo_uvserva_area_academina cuaa on cbub.id_area_cademica_fk = cuaa.id_area_academica;
        ";
        $query = $conn->prepare($sql);
        $query ->execute();
        $result = $query->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }

    public function statusRechazados(){
        $conexion = new DB();
        $id = $this->getId();
        $conn = $conexion->connection();
        $sql = "    
    select
    count(case when cuaa.nombreArea = 'Artes' and cbub.status = 'Rechazado'  then 1 end ) as Status_Rec_A,
    count(case when cuaa.nombreArea = 'Biológico Agropecuaria' and cbub.status = 'Rechazado'  then 1 end ) as Status_Rec_BA,
    count(case when cuaa.nombreArea = 'Ciencias de la Salud' and cbub.status = 'Rechazado'  then 1 end ) as Status_Rec_CS,
    count(case when cuaa.nombreArea = 'Económico Administrativa' and cbub.status = 'Rechazado'  then 1 end ) as Status_Rec_EA,
    count(case when cuaa.nombreArea = 'Humanidades' and cbub.status = 'Rechazado'  then 1 end ) as Status_Rec_H,
    count(case when cuaa.nombreArea = 'Técnica' and cbub.status = 'Rechazado'  then 1 end ) as Status_Rec_T
   from cuo_bitacora_uvserva_bitacora cbub
    inner join cuo_uvserva_area_academina cuaa on cbub.id_area_cademica_fk = cuaa.id_area_academica;
        ";
        $query = $conn->prepare($sql);
        $query ->execute();
        $result = $query->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }

    public function procesoCompleto(){
        $conexion = new DB();
        $id = $this->getId();
        $conn = $conexion->connection();
        $sql = "    
  select
    count(case when cuaa.nombreArea = 'Artes' and cbub.proceso = 'Completo'  then 1 end ) as Pro_Com_A,
    count(case when cuaa.nombreArea = 'Biológico Agropecuaria' and cbub.proceso = 'Completo'  then 1 end ) as Pro_Com_BA,
    count(case when cuaa.nombreArea = 'Ciencias de la Salud' and cbub.proceso = 'Completo'  then 1 end ) as Pro_Com_CS,
    count(case when cuaa.nombreArea = 'Económico Administrativa' and cbub.proceso = 'Completo'  then 1 end ) as Pro_Com_EA,
    count(case when cuaa.nombreArea = 'Humanidades' and cbub.proceso = 'Completo'  then 1 end ) as Pro_Com_H,
    count(case when cuaa.nombreArea = 'Técnica' and cbub.proceso = 'Completo'  then 1 end ) as Pro_Com_T
   from cuo_bitacora_uvserva_bitacora cbub
    inner join cuo_uvserva_area_academina cuaa on cbub.id_area_cademica_fk = cuaa.id_area_academica;
        ";
        $query = $conn->prepare($sql);
        $query ->execute();
        $result = $query->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }

    public function statusAceptados(){
        $conexion = new DB();
        $id = $this->getId();
        $conn = $conexion->connection();
        $sql = "    
    select
    count(case when cuaa.nombreArea = 'Artes' and cbub.status = 'Acetados'  then 1 end ) as Status_Ac_A,
    count(case when cuaa.nombreArea = 'Biológico Agropecuaria' and cbub.status = 'Acetados'  then 1 end ) as Status_Ac_BA,
    count(case when cuaa.nombreArea = 'Ciencias de la Salud' and cbub.status = 'Acetados'  then 1 end ) as Status_Ac_CS,
    count(case when cuaa.nombreArea = 'Económico Administrativa' and cbub.status = 'Acetados'  then 1 end ) as Status_Ac_EA,
    count(case when cuaa.nombreArea = 'Humanidades' and cbub.status = 'Acetados'  then 1 end ) as Status_Ac_H,
    count(case when cuaa.nombreArea = 'Técnica' and cbub.status = 'Acetados'  then 1 end ) as Status_Ac_T
   from cuo_bitacora_uvserva_bitacora cbub
    inner join cuo_uvserva_area_academina cuaa on cbub.id_area_cademica_fk = cuaa.id_area_academica;
        ";
        $query = $conn->prepare($sql);
        $query ->execute();
        $result = $query->fetchAll(PDO::FETCH_OBJ);
        return $result;

    }

    public function filtradorTabla(){
        $conexion = new DB();
        $id = $this->getId();
        $conn = $conexion->connection();
        $sql = "    
    select *
    from cuo_bitacora_uvserva_bitacora cbub
    inner join cuo_uvserva_area_academina cuaa on cbub.id_area_cademica_fk = cuaa.id_area_academica;
        ";
        $query = $conn->prepare($sql);
        $query ->execute();
        $result = $query->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }


    public function valorMax(){
        $conexion = new DB();
        $id = $this->getId();
        $conn = $conexion->connection();
        $sql = "select max(conv_num) maximo from cuo_bitacora_uvserva_bitacora";
        $query = $conn->prepare($sql);
        $query ->execute();
        $result = $query->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }













}