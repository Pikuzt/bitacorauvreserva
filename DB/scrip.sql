/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 5.7.24 : Database - bitacorauvserva
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`bitacorauvserva` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;

USE `bitacorauvserva`;

/*Table structure for table `cuo_bit_tipo_colaboracion` */

DROP TABLE IF EXISTS `cuo_bit_tipo_colaboracion`;

CREATE TABLE `cuo_bit_tipo_colaboracion` (
  `id_tipo_Colaboracion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_colaboracion` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id_tipo_Colaboracion`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `cuo_bit_tipo_colaboracion` */

insert  into `cuo_bit_tipo_colaboracion`(`id_tipo_Colaboracion`,`nombre_colaboracion`) values 
(1,'Artículo '),
(2,'Artículo original'),
(3,'Artículo de revision '),
(4,'Estudio de caso');

/*Table structure for table `cuo_bit_uvservat_seccion` */

DROP TABLE IF EXISTS `cuo_bit_uvservat_seccion`;

CREATE TABLE `cuo_bit_uvservat_seccion` (
  `id_seccion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_sec` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id_seccion`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `cuo_bit_uvservat_seccion` */

insert  into `cuo_bit_uvservat_seccion`(`id_seccion`,`nombre_sec`) values 
(1,'Artículos Científicos'),
(2,'Nota breve');

/*Table structure for table `cuo_bitacora_uvserva_bitacora` */

DROP TABLE IF EXISTS `cuo_bitacora_uvserva_bitacora`;

CREATE TABLE `cuo_bitacora_uvserva_bitacora` (
  `id_bitacora` int(11) NOT NULL AUTO_INCREMENT,
  `num_colaboracion` int(11) DEFAULT NULL,
  `id_envio_ojs` int(11) DEFAULT NULL,
  `anio` int(11) DEFAULT NULL,
  `conv_num` int(11) DEFAULT NULL,
  `similitud` float DEFAULT NULL,
  `lineamiento` varchar(2) COLLATE utf8_spanish_ci DEFAULT NULL,
  `colaboracionIngles` blob,
  `colaboracionEspañol` blob,
  `autores` blob,
  `correo` blob,
  `institucion` blob,
  `dependencia_centroTrabajo` blob,
  `Rep_comite_editorial` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha_recepcion` date DEFAULT NULL,
  `enviado_comite` varchar(2) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha_envio_comiteEdi` date DEFAULT NULL,
  `Asignado_CE` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha_asig_dictadores` date DEFAULT NULL,
  `id_area_cademica_fk` int(11) DEFAULT NULL,
  `id_obs_fk` int(11) DEFAULT NULL,
  `id_seccion_fk` int(11) DEFAULT NULL,
  `id_tipo_col_fk` int(11) DEFAULT NULL,
  `id_pais_fk` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_bitacora`),
  KEY `id_obs_fk` (`id_obs_fk`),
  KEY `id_area_cademica_fk` (`id_area_cademica_fk`),
  KEY `id_seccion_fk` (`id_seccion_fk`),
  KEY `id_tipo_col_fk` (`id_tipo_col_fk`),
  KEY `id_pai_fk` (`id_pais_fk`),
  CONSTRAINT `cuo_bitacora_uvserva_bitacora_ibfk_1` FOREIGN KEY (`id_area_cademica_fk`) REFERENCES `cuo_uvserva_area_academina` (`id_area_academica`),
  CONSTRAINT `cuo_bitacora_uvserva_bitacora_ibfk_2` FOREIGN KEY (`id_obs_fk`) REFERENCES `cuo_uvserva_observatorio` (`id_observatorio`),
  CONSTRAINT `cuo_bitacora_uvserva_bitacora_ibfk_3` FOREIGN KEY (`id_seccion_fk`) REFERENCES `cuo_bit_uvservat_seccion` (`id_seccion`),
  CONSTRAINT `cuo_bitacora_uvserva_bitacora_ibfk_4` FOREIGN KEY (`id_tipo_col_fk`) REFERENCES `cuo_bit_tipo_colaboracion` (`id_tipo_Colaboracion`),
  CONSTRAINT `cuo_bitacora_uvserva_bitacora_ibfk_5` FOREIGN KEY (`id_pais_fk`) REFERENCES `paises` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `cuo_bitacora_uvserva_bitacora` */

insert  into `cuo_bitacora_uvserva_bitacora`(`id_bitacora`,`num_colaboracion`,`id_envio_ojs`,`anio`,`conv_num`,`similitud`,`lineamiento`,`colaboracionIngles`,`colaboracionEspañol`,`autores`,`correo`,`institucion`,`dependencia_centroTrabajo`,`Rep_comite_editorial`,`fecha_recepcion`,`enviado_comite`,`fecha_envio_comiteEdi`,`Asignado_CE`,`fecha_asig_dictadores`,`id_area_cademica_fk`,`id_obs_fk`,`id_seccion_fk`,`id_tipo_col_fk`,`id_pais_fk`) values 
(1,1,2689,2020,10,6,'si','Falta título inglés ','Presencia de parásitos intestinales en mesas de expendios de alimentos ubicados en las inmediaciones de la UCS región Xalapa de la Universidad Veracruzana	','\"Sandra Luz González Herrera, \r\nMargarita Lozada Méndez (contacto)\"\r\n','mlozada@uv.mx\r\n','Universidad Veracruzana','S/D','Dra. Catalina Cervantes Ortega','2020-01-16',NULL,NULL,NULL,NULL,3,5,1,1,146),
(2,2,2696,2020,10,2,'si','Falta título inglés ','Importancia de un monitoreo hidrográfico a largo plazo','\"David Salas-Monreal (contacto); \r\nGabriela Athie; \r\n Mayra Lorena Riverón-Enzástiga\"\r\n','davsalas@uv.mx\r\n','Universidad Veracruzana','Instituto de Ciencias Marinas y Pesqueras, región Veracruz\r\n','Mtro. José Luis Sánchez Castro','2020-05-01',NULL,NULL,NULL,NULL,2,5,2,1,146),
(3,3,2724,2020,10,1,'si','LBH land-based housing, alternative for the social production of housing.','Vivienda a base de tierra BTC, alternativa para la producción social de la vivienda','Álvaro Hernández Santiago; Diego Torres Hoyos (contacto); Heidi LizbethMonroy Carranza; Jesús Ceballos Vargas\r\n','alhernandez@uv.mx; \r\ndtorres@uv.mx; \r\nhmonroy@uv.mx; \r\njceballos@uv.mx\r\n\r\n','Universidad Veracruzana','Facultad de Arquitectura, región Poza Rica-Tuxpan\r\n','Dra. Alma Vázquez Luna','2020-07-22',NULL,NULL,NULL,NULL,6,1,1,2,146),
(4,4,2739,2020,10,6,'si','The political device of tourism and the increase in local contributions in Orizaba Veracruz during the year 2013 to 2017','El aparato político de lo turístico y el aumento de las contribuciones locales en Orizaba Veracruz durante el año 2013 al 2017','Marco Antonio Muñoz Madrid (contacto)\r\n','marcomunoz03@uv.mx\r\n','Universidad Veracruzana',' Coordinación Académica Regional de Enseñanza Abierta\r\n','Dra. Ana Cecilia Travieso Bello','2020-08-24',NULL,NULL,NULL,NULL,4,5,1,2,146),
(5,5,2741,2020,10,31,'si','Comparison and prevalence of risk factors in students of the Universidad Veracruzana, Xalapa region, México','Comparación y prevalencia de factores de riesgo en estudiantes de la universidad veracruzana, región Xalapa, México','José Guadalupe García González\r\n','guadagarcia@uv.mx\r\n','Universidad Veracruzana','Centro para el Desarrollo Humano e Integral de los Universitarios\r\n','Dra. Catalina Cervantes Ortega','2020-07-19',NULL,NULL,NULL,NULL,3,5,1,2,146),
(6,6,2744,2020,11,19,'si','S/D','Transformando la enseñanza de la Historia a través del Juego. Una reflexión teórica-práctica','Jorge Alejandro Trejo Alarcón \r\n\r\n','jorge.trejo.alarco@gmail.com\r\n\r\n','Colegio Real Victoria','Colegio Real Victoria','S/D','2020-09-11',NULL,NULL,NULL,NULL,5,5,1,3,146),
(7,7,2747,2021,11,34,'si','Mobile applications as a technological resource for teachers in the teaching-learning process of the Technological University of Gutiérrez Zamora','Aplicaciones móviles como recurso tecnológico para docentes en el proceso de enseñanza-aprendizaje de la Universidad Tecnológica de Gutiérrez Zamora','Jesús Alexander Loza Cruz\r\n\r\n','jloza@uv.mx\r\n\r\n','Universidad Veracruzana','Facultad de Pedagogía, región Poza Rica -Tuxpan\r\n\r\n','S/D','2020-08-04',NULL,NULL,NULL,NULL,5,5,1,2,146),
(8,7,2747,2021,11,34,'si','Mobile applications as a technological resource for teachers in the teaching-learning process of the Technological University of Gutiérrez Zamora','Aplicaciones móviles como recurso tecnológico para docentes en el proceso de enseñanza-aprendizaje de la Universidad Tecnológica de Gutiérrez Zamora','Jesús Alexander Loza Cruz\r\n\r\n','jloza@uv.mx\r\n\r\n','Universidad Veracruzana','Facultad de Pedagogía, región Poza Rica -Tuxpan\r\n\r\n','S/D','2020-08-04',NULL,NULL,NULL,NULL,5,5,1,2,146),
(9,8,2748,2020,11,19,'si','From aggressive periodontitis to periodontitis: modifications in its classification and treatment through the case report.','De periodontitis agresiva a periodontitis: modificaciones en su clasificación y tratamiento a través del reporte de caso','\"Michelle Rustrián-Campillo ;\r\nRussell-Hernández Miguel Hazael ;\r\n  Miguel Eric García-Rivera\r\nNachón-García María Gabriela4\"\r\n','\"michazaelina0508@gmail.com ;\r\nhazael-miqel@hotmail.com ;\r\nmiguegarcia@uv.mx ;\r\ngnachon@uv.mx\"\r\n','Universidad Veracruzana','Facultad de Odontología/Instituto de Ciencias de la Salud\r\n','Dra. Catalina Cervantes Ortega ','2020-09-21',NULL,NULL,NULL,NULL,3,5,1,4,146),
(10,9,2757,2021,11,11,'si','Evaluation of the suitability of the graduate profile in Clinical Chemist in Universidad Veracruzana Campus Veracruz-Boca del Río','Evaluación de la idoneidad del perfil de egreso del Químico Clínico de la Universidad Veracruzana Campus Veracruz-Boca del Río','\"Fernando Ciro López Fentanes ;\r\nJavier Sosa Rueda\"\r\n','\"felopez@uv.mx ;\r\nsosa_0701@hotmail.com\r\n\"\r\n','Universidad Veracruzana','Facultad de Bioanálisis, región Veracruz\r\n','Dra. Catalina Cervantes Ortega ','2020-11-06',NULL,NULL,NULL,NULL,3,5,1,2,146),
(11,10,2758,2021,11,28,'si','Falta título inglés ','\"La importancia del espacio público en el mejoramiento socioambiental \"','\"Diego Arturo Torres Hoyos;\r\nAlvaro Hernandez Santiago;\r\n Jesús Ceballos Vargas\"\r\n','\"dtorres@uv.mx;\r\nalvahernandez@uv.mx;\r\njceballos@uv.mx\"\r\n','Universidad Veracruzana','Facultad de Arquitectura. región Poza Rica-Tuxpan\r\n','Dra. Alma Vázquez Luna','2021-01-07',NULL,NULL,NULL,NULL,6,1,1,1,146);

/*Table structure for table `cuo_uvserva_area_academina` */

DROP TABLE IF EXISTS `cuo_uvserva_area_academina`;

CREATE TABLE `cuo_uvserva_area_academina` (
  `id_area_academica` int(11) NOT NULL AUTO_INCREMENT,
  `nombreArea` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id_area_academica`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `cuo_uvserva_area_academina` */

insert  into `cuo_uvserva_area_academina`(`id_area_academica`,`nombreArea`) values 
(1,'Artes'),
(2,'Biológico Agropecuaria'),
(3,'Ciencias de la Salud '),
(4,'Económico Administrativa'),
(5,'Humanidades'),
(6,'Técnica');

/*Table structure for table `cuo_uvserva_observatorio` */

DROP TABLE IF EXISTS `cuo_uvserva_observatorio`;

CREATE TABLE `cuo_uvserva_observatorio` (
  `id_observatorio` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_observa` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id_observatorio`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `cuo_uvserva_observatorio` */

insert  into `cuo_uvserva_observatorio`(`id_observatorio`,`nombre_observa`) values 
(1,'OURBE'),
(2,'OABCC'),
(3,'OUVMujeres/OPC'),
(4,'¿OUU?'),
(5,'---');

/*Table structure for table `cuo_uvserva_revision` */

DROP TABLE IF EXISTS `cuo_uvserva_revision`;

CREATE TABLE `cuo_uvserva_revision` (
  `id_revision` int(11) NOT NULL AUTO_INCREMENT,
  `id_art_fk` int(11) DEFAULT NULL,
  `nombre_revision` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha_envio` date DEFAULT NULL,
  `fecha_recepcion` date DEFAULT NULL,
  `recomendacion` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  `constancia_enviada` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id_revision`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `cuo_uvserva_revision` */

/*Table structure for table `paises` */

DROP TABLE IF EXISTS `paises`;

CREATE TABLE `paises` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iso` char(2) DEFAULT NULL,
  `nombre` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=241 DEFAULT CHARSET=latin1;

/*Data for the table `paises` */

insert  into `paises`(`id`,`iso`,`nombre`) values 
(1,'AF','Afganistán'),
(2,'AX','Islas Gland'),
(3,'AL','Albania'),
(4,'DE','Alemania'),
(5,'AD','Andorra'),
(6,'AO','Angola'),
(7,'AI','Anguilla'),
(8,'AQ','Antártida'),
(9,'AG','Antigua y Barbuda'),
(10,'AN','Antillas Holandesas'),
(11,'SA','Arabia Saudí'),
(12,'DZ','Argelia'),
(13,'AR','Argentina'),
(14,'AM','Armenia'),
(15,'AW','Aruba'),
(16,'AU','Australia'),
(17,'AT','Austria'),
(18,'AZ','Azerbaiyán'),
(19,'BS','Bahamas'),
(20,'BH','Bahréin'),
(21,'BD','Bangladesh'),
(22,'BB','Barbados'),
(23,'BY','Bielorrusia'),
(24,'BE','Bélgica'),
(25,'BZ','Belice'),
(26,'BJ','Benin'),
(27,'BM','Bermudas'),
(28,'BT','Bhután'),
(29,'BO','Bolivia'),
(30,'BA','Bosnia y Herzegovina'),
(31,'BW','Botsuana'),
(32,'BV','Isla Bouvet'),
(33,'BR','Brasil'),
(34,'BN','Brunéi'),
(35,'BG','Bulgaria'),
(36,'BF','Burkina Faso'),
(37,'BI','Burundi'),
(38,'CV','Cabo Verde'),
(39,'KY','Islas Caimán'),
(40,'KH','Camboya'),
(41,'CM','Camerún'),
(42,'CA','Canadá'),
(43,'CF','República Centroafricana'),
(44,'TD','Chad'),
(45,'CZ','República Checa'),
(46,'CL','Chile'),
(47,'CN','China'),
(48,'CY','Chipre'),
(49,'CX','Isla de Navidad'),
(50,'VA','Ciudad del Vaticano'),
(51,'CC','Islas Cocos'),
(52,'CO','Colombia'),
(53,'KM','Comoras'),
(54,'CD','República Democrática del Congo'),
(55,'CG','Congo'),
(56,'CK','Islas Cook'),
(57,'KP','Corea del Norte'),
(58,'KR','Corea del Sur'),
(59,'CI','Costa de Marfil'),
(60,'CR','Costa Rica'),
(61,'HR','Croacia'),
(62,'CU','Cuba'),
(63,'DK','Dinamarca'),
(64,'DM','Dominica'),
(65,'DO','República Dominicana'),
(66,'EC','Ecuador'),
(67,'EG','Egipto'),
(68,'SV','El Salvador'),
(69,'AE','Emiratos Árabes Unidos'),
(70,'ER','Eritrea'),
(71,'SK','Eslovaquia'),
(72,'SI','Eslovenia'),
(73,'ES','España'),
(74,'UM','Islas ultramarinas de Estados Unidos'),
(75,'US','Estados Unidos'),
(76,'EE','Estonia'),
(77,'ET','Etiopía'),
(78,'FO','Islas Feroe'),
(79,'PH','Filipinas'),
(80,'FI','Finlandia'),
(81,'FJ','Fiyi'),
(82,'FR','Francia'),
(83,'GA','Gabón'),
(84,'GM','Gambia'),
(85,'GE','Georgia'),
(86,'GS','Islas Georgias del Sur y Sandwich del Sur'),
(87,'GH','Ghana'),
(88,'GI','Gibraltar'),
(89,'GD','Granada'),
(90,'GR','Grecia'),
(91,'GL','Groenlandia'),
(92,'GP','Guadalupe'),
(93,'GU','Guam'),
(94,'GT','Guatemala'),
(95,'GF','Guayana Francesa'),
(96,'GN','Guinea'),
(97,'GQ','Guinea Ecuatorial'),
(98,'GW','Guinea-Bissau'),
(99,'GY','Guyana'),
(100,'HT','Haití'),
(101,'HM','Islas Heard y McDonald'),
(102,'HN','Honduras'),
(103,'HK','Hong Kong'),
(104,'HU','Hungría'),
(105,'IN','India'),
(106,'ID','Indonesia'),
(107,'IR','Irán'),
(108,'IQ','Iraq'),
(109,'IE','Irlanda'),
(110,'IS','Islandia'),
(111,'IL','Israel'),
(112,'IT','Italia'),
(113,'JM','Jamaica'),
(114,'JP','Japón'),
(115,'JO','Jordania'),
(116,'KZ','Kazajstán'),
(117,'KE','Kenia'),
(118,'KG','Kirguistán'),
(119,'KI','Kiribati'),
(120,'KW','Kuwait'),
(121,'LA','Laos'),
(122,'LS','Lesotho'),
(123,'LV','Letonia'),
(124,'LB','Líbano'),
(125,'LR','Liberia'),
(126,'LY','Libia'),
(127,'LI','Liechtenstein'),
(128,'LT','Lituania'),
(129,'LU','Luxemburgo'),
(130,'MO','Macao'),
(131,'MK','ARY Macedonia'),
(132,'MG','Madagascar'),
(133,'MY','Malasia'),
(134,'MW','Malawi'),
(135,'MV','Maldivas'),
(136,'ML','Malí'),
(137,'MT','Malta'),
(138,'FK','Islas Malvinas'),
(139,'MP','Islas Marianas del Norte'),
(140,'MA','Marruecos'),
(141,'MH','Islas Marshall'),
(142,'MQ','Martinica'),
(143,'MU','Mauricio'),
(144,'MR','Mauritania'),
(145,'YT','Mayotte'),
(146,'MX','México'),
(147,'FM','Micronesia'),
(148,'MD','Moldavia'),
(149,'MC','Mónaco'),
(150,'MN','Mongolia'),
(151,'MS','Montserrat'),
(152,'MZ','Mozambique'),
(153,'MM','Myanmar'),
(154,'NA','Namibia'),
(155,'NR','Nauru'),
(156,'NP','Nepal'),
(157,'NI','Nicaragua'),
(158,'NE','Níger'),
(159,'NG','Nigeria'),
(160,'NU','Niue'),
(161,'NF','Isla Norfolk'),
(162,'NO','Noruega'),
(163,'NC','Nueva Caledonia'),
(164,'NZ','Nueva Zelanda'),
(165,'OM','Omán'),
(166,'NL','Países Bajos'),
(167,'PK','Pakistán'),
(168,'PW','Palau'),
(169,'PS','Palestina'),
(170,'PA','Panamá'),
(171,'PG','Papúa Nueva Guinea'),
(172,'PY','Paraguay'),
(173,'PE','Perú'),
(174,'PN','Islas Pitcairn'),
(175,'PF','Polinesia Francesa'),
(176,'PL','Polonia'),
(177,'PT','Portugal'),
(178,'PR','Puerto Rico'),
(179,'QA','Qatar'),
(180,'GB','Reino Unido'),
(181,'RE','Reunión'),
(182,'RW','Ruanda'),
(183,'RO','Rumania'),
(184,'RU','Rusia'),
(185,'EH','Sahara Occidental'),
(186,'SB','Islas Salomón'),
(187,'WS','Samoa'),
(188,'AS','Samoa Americana'),
(189,'KN','San Cristóbal y Nevis'),
(190,'SM','San Marino'),
(191,'PM','San Pedro y Miquelón'),
(192,'VC','San Vicente y las Granadinas'),
(193,'SH','Santa Helena'),
(194,'LC','Santa Lucía'),
(195,'ST','Santo Tomé y Príncipe'),
(196,'SN','Senegal'),
(197,'CS','Serbia y Montenegro'),
(198,'SC','Seychelles'),
(199,'SL','Sierra Leona'),
(200,'SG','Singapur'),
(201,'SY','Siria'),
(202,'SO','Somalia'),
(203,'LK','Sri Lanka'),
(204,'SZ','Suazilandia'),
(205,'ZA','Sudáfrica'),
(206,'SD','Sudán'),
(207,'SE','Suecia'),
(208,'CH','Suiza'),
(209,'SR','Surinam'),
(210,'SJ','Svalbard y Jan Mayen'),
(211,'TH','Tailandia'),
(212,'TW','Taiwán'),
(213,'TZ','Tanzania'),
(214,'TJ','Tayikistán'),
(215,'IO','Territorio Británico del Océano Índico'),
(216,'TF','Territorios Australes Franceses'),
(217,'TL','Timor Oriental'),
(218,'TG','Togo'),
(219,'TK','Tokelau'),
(220,'TO','Tonga'),
(221,'TT','Trinidad y Tobago'),
(222,'TN','Túnez'),
(223,'TC','Islas Turcas y Caicos'),
(224,'TM','Turkmenistán'),
(225,'TR','Turquía'),
(226,'TV','Tuvalu'),
(227,'UA','Ucrania'),
(228,'UG','Uganda'),
(229,'UY','Uruguay'),
(230,'UZ','Uzbekistán'),
(231,'VU','Vanuatu'),
(232,'VE','Venezuela'),
(233,'VN','Vietnam'),
(234,'VG','Islas Vírgenes Británicas'),
(235,'VI','Islas Vírgenes de los Estados Unidos'),
(236,'WF','Wallis y Futuna'),
(237,'YE','Yemen'),
(238,'DJ','Yibuti'),
(239,'ZM','Zambia'),
(240,'ZW','Zimbabue');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
